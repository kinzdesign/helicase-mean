#!/bin/bash
echo 'Pushing production data backup to development...'
echo ' - database structure'
echo ' - seed data'
echo ' - production participant data backup'
cat sql/prod.structure.sql sql/prod.seeds.sql sql/prod.data.sql | \
mysql --comments \
	-h $DEV_HOST \
	-P $DEV_PORT \
	-u $DEV_MASTER_USER \
	-p$DEV_MASTER_PASS \
	$DEV_DATABASE
echo 'Done!'
echo