#!/bin/bash
echo
echo 'Dumping database structure from staging...'
mysqldump \
	--routines -d --skip-dump-date \
	-h $STAGE_HOST \
	-P $STAGE_PORT \
	-u $STAGE_MASTER_USER \
	-p$STAGE_MASTER_PASS \
	$STAGE_DATABASE \
	--ssl-ca=config/amazon-rds-ca-cert.pem \
	--ssl-mode=VERIFY_IDENTITY \
| sed \
	-e '/!50003 SET sql_mode/d' \
	-e '/Host: .* Database: /d' \
	-e '/Server version/d' \
	-e '/Dumping routines for database /d' \
	-e 's/DEFINER[ ]*=[ ]*[^*]*FUNCTION/FUNCTION/' \
	-e 's/DEFINER[ ]*=[ ]*[^*]*PROCEDURE/PROCEDURE/' \
	-e 's/AUTO_INCREMENT=[0-9]*\s*//' \
	-e 's/DEFINER[ ]*=[ ]*[^*]*\*/\*/' > sql/stage.structure.sql
	-e 's/`helicase_dev`\.//g' \
echo 'Done!'
echo
echo 'Dumping seed data from staging...'
mysqldump \
	--no-create-info --skip-dump-date \
	-h $STAGE_HOST \
	-P $STAGE_PORT \
	-u $STAGE_MASTER_USER \
	-p$STAGE_MASTER_PASS \
	$STAGE_DATABASE \
	--ssl-ca=config/amazon-rds-ca-cert.pem \
	--ssl-mode=VERIFY_IDENTITY \
	experiences pathways badges quizzes questions answers videos \
| sed \
	-e '/!50003 SET sql_mode/d' \
	-e '/Host: .* Database: /d' \
	-e '/Server version/d' \
	-e '/Dumping routines for database /d' \
	-e 's$VALUES ($VALUES\n($g' \
	-e 's$),($),\n($g' \
	-e 's/`helicase_dev`\.//g' \
> sql/stage.seeds.sql
echo 'Done!'
echo
echo 'Dumping participant data from staging...'
mysqldump --no-create-info \
	-h $STAGE_HOST \
	-P $STAGE_PORT \
	-u $STAGE_MASTER_USER \
	-p$STAGE_MASTER_PASS \
	$STAGE_DATABASE \
	--ssl-ca=config/amazon-rds-ca-cert.pem \
	--ssl-mode=VERIFY_IDENTITY \
	participants q_participant_quiz responses responses_multi interactions \
| sed \
	-e '/!50003 SET sql_mode/d' \
	-e '/Host: .* Database: /d' \
	-e '/Server version/d' \
	-e '/Dumping routines for database /d' \
	-e 's$VALUES ($VALUES\n($g' \
	-e 's$),($),\n($g' \
	-e 's/`helicase_dev`\.//g' \
> sql/stage.data.sql
echo 'Done!'
echo