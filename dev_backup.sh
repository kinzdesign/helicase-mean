#!/bin/bash
echo
echo 'Dumping database structure from development...'
mysqldump \
	--routines -d --skip-dump-date \
	-h $DEV_HOST \
	-P $DEV_PORT \
	-u $DEV_MASTER_USER \
	-p$DEV_MASTER_PASS \
	$DEV_DATABASE \
| sed \
	-e '/!50003 SET sql_mode/d' \
	-e '/Host: .* Database: /d' \
	-e '/Server version/d' \
	-e '/Dumping routines for database /d' \
	-e 's$VALUES ($VALUES\n($g' \
	-e 's$),($),\n($g' \
	-e 's/DEFINER[ ]*=[ ]*[^*]*FUNCTION/FUNCTION/' \
	-e 's/DEFINER[ ]*=[ ]*[^*]*PROCEDURE/PROCEDURE/' \
	-e 's/AUTO_INCREMENT=[0-9]*\s*//' \
	-e 's/DEFINER[ ]*=[ ]*[^*]*\*/\*/' \
	-e 's/`helicase`\.//g' \
> sql/structure.sql
echo 'Done!'
echo
echo 'Dumping seed data from development...'
mysqldump \
	--no-create-info --skip-dump-date \
	-h $DEV_HOST \
	-P $DEV_PORT \
	-u $DEV_MASTER_USER \
	-p$DEV_MASTER_PASS \
	$DEV_DATABASE \
	experiences pathways badges quizzes questions answers videos \
| sed \
	-e '/!50003 SET sql_mode/d' \
	-e '/Host: .* Database: /d' \
	-e '/Server version/d' \
	-e '/Dumping routines for database /d' \
	-e 's$VALUES ($VALUES\n($g' \
	-e 's$),($),\n($g' \
	-e 's/`helicase`\.//g' \
> sql/seeds.sql
echo 'Done!'
echo
echo 'Dumping participant data from development...'
mysqldump \
	--no-create-info \
	-h $DEV_HOST \
	-P $DEV_PORT \
	-u $DEV_MASTER_USER \
	-p$DEV_MASTER_PASS \
	$DEV_DATABASE \
	participants q_participant_quiz responses responses_multi interactions \
| sed \
	-e '/!50003 SET sql_mode/d' \
	-e '/Host: .* Database: /d' \
	-e '/Server version/d' \
	-e '/Dumping routines for database /d' \
	-e 's$VALUES ($VALUES\n($g' \
	-e 's$),($),\n($g' \
	-e 's/`helicase`\.//g' \
> sql/dev.data.sql
echo 'Done!'
echo
echo 'Dumping permissions from development...'
mysqldump \
	--opt --skip-dump-date \
	-h $DEV_HOST \
	-P $DEV_PORT \
	-u $DEV_MASTER_USER \
	-p$DEV_MASTER_PASS \
	mysql user \
| sed \
	-e 's$VALUES ($VALUES\n($g' \
	-e 's$),($),\n($g' \
	-e 's/`helicase`\.//g' \
> sql/dev.permissions.sql
echo 'Done!'
echo