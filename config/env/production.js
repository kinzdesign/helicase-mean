'use strict';

module.exports = {
  db: process.env.MONGODB_URI,
  hostname: 'https://kr-research.herokuapp.com/',
  app: {
    name: 'Helicase'
  },
  logging: {
    format: 'combined'
  },
  secret: 'jA$iu9oAaOKPgst4XrgY1%hFKMasf5wI'
};
