'use strict';

module.exports = {
  db: process.env.MONGODB_URI,
  debug: true,
  logging: {
    format: 'tiny'
  },
  hostname: 'http://localhost:3000',
  app: {
    name: 'Helicase-dev'
  },
  secret: 'd9dQ1#I%1s!g1VgKVvmHNkQsCZM2!4Ux'
};
