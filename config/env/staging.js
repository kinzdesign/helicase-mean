'use strict';

module.exports = {
  db: process.env.MONGODB_URI,
  debug: true,
  logging: {
    format: 'tiny'
  },
  hostname: 'https://kr-research-stage.herokuapp.com/',
  app: {
    name: 'Helicase-staging'
  },
  secret: 'EU#L8v2xJV0Xp$vxS1a4yKf@vHmdvMmF'
};
