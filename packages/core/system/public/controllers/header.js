'use strict';

angular.module('mean.system').controller('HeaderController', ['$scope', '$rootScope', 'MeanUser', '$state',
  function($scope, $rootScope, MeanUser, $state) {
    
    var vm = this;

    vm.hdrvars = {
      user: MeanUser.user
    };

    $rootScope.$on('loggedin', function() {
      vm.hdrvars = {
        user: MeanUser.user
      };
    });


  }
]);
