'use strict';
var $$interval;
var user;
var videoIds = {};
// adapted from http://stackoverflow.com/a/24228604
angular.module('mean.system').directive('youtubeApi', [
  '$rootScope', '$interval', 'MeanUser', 
  function($rootScope, $interval, MeanUser) {
    $$interval = $interval;
    user = MeanUser;

    $rootScope.$on('submitting', function() {
      for(var youtube in players) {
        var player = players[youtube];
        if(player) {
          // stop and log interaction if playing
          if(player.getPlayerState() == 1) {
            player.stopVideo();
            q_vid.push({
              track: 'youtube',
              clientStamp: Date.now(),
              video: videoIds[youtube],
              infoInt: player.getCurrentTime() * 1000,
              infoBit: 0, // not playing
              infoByte: 9 // not defined in YouTube API states
            });
          }
          // free player resources
          player.destroy();
          players[youtube] = false;
        }
      }
      // post interaction data
      viewTick();
    });

    return {
      link: function($scope, element, attrs) {
        // trigger when number of player divs changes
        var watch = $scope.$watch(function() {
          return $('[data-youtube]').length;
        }, function() {
          // Wait for templates to render
          $scope.$evalAsync(function() {
            // Finally, directives are evaluated
            // and templates are rendered here

            // if there are videos on the page, load the API
            if($('[data-youtube]').length > 0)
              loadYoutubeIframeApi();
          });
        });
      }
    };
  }
]);

// queue views for periodic posting
var q_vid = new Array();
// keep track of time when last polled
var last_polled = {};
// injects YouTube API <script> tag into DOM
function loadYoutubeIframeApi() {
  // if script already loaded, trigger ready
  if($('script[src="https://www.youtube.com/iframe_api"]').length) {
// console.log('in loadYoutubeIframeApi, API alreaddy loaded');
    if(typeof YT !== 'undefined') window.onYouTubeIframeAPIReady();
  // otherwise, inject api script
  } else {
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  }
}

// create players
var players = { };
window.onYouTubeIframeAPIReady = function() {
  // iterate over videos on page
  $('[data-youtube]').each(function() {
    var $this = $(this);
    var youtube = $this.attr('data-youtube');
    videoIds[youtube] = $this.attr('data-video');
    // create player if not exist and API loaded
    if(!players[youtube] && typeof YT !== 'undefined') {
      players[youtube] = new YT.Player(
        $this.attr('id'),
        {
          videoId: youtube,
          playerVars: { 
            'autoplay': (($this.attr('data-autoplay') || '').toLowerCase() == 'true')
          },
          events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
        }
      );
    }
  });
  $$interval(viewTick, 1000);
}

// The API will call this function when the video player is ready.
function onPlayerReady(event) {
  // begin polling at ~4Hz
  $$interval(pollVideo, 250, 0, true, event.target);
}

// The API calls this function when the player's state changes.
function onPlayerStateChange(event) {
  enqueueView(event.target);
}

function viewTick() {
// console.log('in viewTick', q_vid);
  // if there's anything to post
  if(q_vid.length > 0) {
    // clone queue 
    var q_tmp = q_vid;
    // replace with empty queue
    q_vid = new Array();
    // post views to server
    user.recordInteractions(q_tmp, function(){ });
  }
}

function enqueueView(player) {
  // ensure queue bucket exists for this video
  var youtube = player.getVideoData()['video_id'];
  q_vid = q_vid || new Array();
  // create item to enqueue
  var tmp = {
    track: 'youtube',
    clientStamp: Date.now(),
    video: videoIds[youtube],
    infoInt: player.getCurrentTime() * 1000,
    infoByte: player.getPlayerState(),
    infoBit: player.getPlayerState() == 1
  };
  // denote whether or not the video is playing
  tmp.infoBit = tmp.infoByte == 1;
  // if state is paused (occurs on seek)
  if(tmp.infoByte == 2) {
    // add a view event with last known play time to 
    // record time before seeking using special status = 8
    q_vid.push({
      track: 'youtube',
      clientStamp: tmp.clientStamp,
      video: tmp.video,
      infoInt: last_polled[youtube] * 1000,
      // this playerState is higher than YouTube defined states
      infoByte: 8,
      // denote that is isn't actively playing
      infoBit: 0
    });
  }
  // enqueue and return it
  q_vid.push(tmp);
// console.log('in enqueueView', q_vid);
  return tmp;
}

// onPlayerStateChange does not report current time before seeking
// have to poll periodically to record current time
var pollVideo = function(player) {
  // see if the selected player is playing
  if(player.getPlayerState() == 1) {
    // if playing, record the video time
    last_polled[player.getVideoData()['video_id']] =
      player.getCurrentTime();
  }
}