'use strict';
// adapted from http://jsfiddle.net/az7uukk6/1/
var user;
angular.module('mean.system').directive('interactableBindHtml', [
  'MeanUser',
  function(MeanUser) { 
    user = MeanUser;
    return {
      link: function($scope,$element,attrs) {
        $scope.$watch(attrs.ngBindHtml, function(newvalue) {
          // runs after ng-bind-html completes
          $element.find('.track-click').on('click', function(event) {
            user.recordInteraction({
              track: 'click',
              infoText: event.target.attributes.href.value
            }, function(err, res) {
// console.log('interactableBindHtml: recorded click', event.target.attributes.href.value, err || res);
              // do nothing
            });
          });   
        });               
      }
    };
  }
]);