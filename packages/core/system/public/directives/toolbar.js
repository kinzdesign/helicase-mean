'use strict';

angular.module('mean.system').directive('toolbar', function() { 
  return { 
    restrict: 'E', 
    templateUrl: '/system/directives/toolbar.html' 
  }; 
});