'use strict';

/*
 * Defining the Package
 */
var mean = require('meanio'),
  Module = mean.Module;

function MeanUserKlass () {
  Module.call(this, 'users');
  this.auth = null;
}

MeanUserKlass.prototype = Object.create(Module.prototype,{constructor:{
  value:MeanUserKlass,
  configurable: false,
  enumerable: false,
  writable: false
}});

var MeanUser = new MeanUserKlass();

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
MeanUser.register(function(app, database) {

    MeanUser.auth = require('./authorization');

    mean.register('auth', MeanUser.auth);

    MeanUser.aggregateAsset('css', 'animation.css');
    MeanUser.aggregateAsset('css', 'badgeChooser.css');
    MeanUser.aggregateAsset('css', 'continue.css');
    MeanUser.aggregateAsset('css', 'genotype.css');
    MeanUser.aggregateAsset('css', 'likertScale.css');
    MeanUser.aggregateAsset('css', 'scoreboard.css');
    MeanUser.aggregateAsset('css', 'thanks.css');
    MeanUser.aggregateAsset('css', 'toolbar.css');
    MeanUser.aggregateAsset('css', 'videoList.css');

    //We enable routing. By default the Package Object is passed to the routes
    MeanUser.routes(app, MeanUser.auth, database);

    MeanUser.angularDependencies(['mean.system', 'ngSanitize']);

    MeanUser.events.defaultData({
        type: 'user'
    });

    return MeanUser;
});
