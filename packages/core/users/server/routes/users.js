'use strict';

module.exports = function(MeanUser, app, auth, database) {

  // User routes use users controller
  var users = require('../controllers/users')(MeanUser);

  // gets participant ID
  app.route('/api/users/me')
    .get(users.me);

  app.route('/api/me')
    .get(users.me);

  app.route('/api/respond')
    .post(users.me);

  app.route('/api/interaction')
    .post(users.interaction);

  app.route('/api/interactions')
    .post(users.interactions);

  // Setting up the userId param
  app.param('userId', users.user);
};