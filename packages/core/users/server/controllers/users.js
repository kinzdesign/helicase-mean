'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
  mysql = require('mysql');

// mysql connection pool
var pool = false;

var processDbResults = function(err, rows, values) {
    // format and output results
    values = values || {};

    // user
    if(rows[0] && rows[0].length > 0) {
        values.user             = rows[0][0];
    }
    // current quiz w/ questions
    if(rows[1] && rows[1].length > 0) {
        values.quiz             = rows[1][0];
        values.quiz.questions   = [];
        _.forEach(rows[2], function(q) {
            values.quiz.questions.push(q.question);
        });
    }
    // current question w/ answers
    if(rows[3]) {
        // emit single question
        if(rows[3].length == 1) {
            delete values.questions;
            values.question         = rows[3][0];
            // attach answers to question
            values.question.multiAnswers = rows[4];
        // emit multiple questions
        } else if(rows[3].length > 1) {
            delete values.question;
            values.questions        = rows[3];
            // assign answers to questions
            values.questions.forEach(function(q) {
                q.multiAnswers = [];
                rows[4].forEach(function(a) {
                    if(a['question'] == q['question']) {
                        q.multiAnswers.push(a);
                    }
                });
            });
        }
    }
    // current response(s)
    if(rows[5]) {
        if(rows[5].length == 1) {
            delete values.responses;
            values.response         = rows[5][0];
        } else if (rows[5].length > 1) {
            delete values.response;
            values.responses        = rows[5];
        }
    }
    // badges
    if(rows[6] && rows[6].length > 0) {
        // don't emit badges if there's an active quiz
        if(!values.quiz)
            // only emit badges if some are incomplete
            _.forEach(rows[6], function(q) {
                if(q.completed == 0)
                    values.badges   = rows[6];
        });
    }
    // videos
    if(rows[7] && rows[7].length > 0) {
        values.videos           = rows[7];
    }
    // scores
    if(rows[8] && rows[8].length > 0 && !rows[8][0].skip) {
        if(rows[8].length == 1)
            values.scores           = rows[8][0];
        else
            values.scores           = rows[8];
    }
    // quiz scores
    if(rows[9] && rows[9].length > 0 && !rows[9][0].skip) {
        values.quizScores       = rows[9];
    }
    // last quiz responses
    if(rows[10] && rows[10].length > 0 && !rows[10][0].skip) {
        values.lastQuiz         = rows[10];
    }
    // leaderboard
    if(rows[11] && rows[11].length > 0 && !rows[11][0].skip) {
        values.leaderboard      = rows[11];
    }

    // clean up
    if(values.question) {
        // remove likert fields if not applicable
        if( !values.question.showLikert ||
            (values.question.renderLikertAfter && (!values.response || !values.response.answer) && !values.question.concurrentLikertMulti)) {
            delete values.question.likertIntroHtml;
            delete values.question.likertMaxLabel;
            delete values.question.likertMinLabel;
        }
        // remove multiple choice if not applicable
        if( !values.question.showMulti ||
            (values.question.renderLikertBefore && (!values.response || !values.response.likert) && !values.question.concurrentLikertMulti)) {
            delete values.question.multiIntroHtml;
            delete values.question.multiAnswers;
        }
    }
    return values;
}

var fromLastToLastExclusive = function(s, fromLast, toLast) {
    var fromIndex = s.lastIndexOf(fromLast) + fromLast.length;
    var toIndex = s.lastIndexOf(toLast);
    if(toIndex < 0) toIndex = s.length;
    return s.substring(fromIndex, toIndex);
}

module.exports = function(MeanUser, config) {

    // configure database pool if not initialized
    pool = pool || mysql.createPool(process.env.MYSQL_DATABASE_URL);

    return {

        me: function(req, res) {
            // get posted values
            var values = req.response || {};
            values = _.extend(values, req.body);
            // convert strings to dates
            if(values.consented) values.consented = new Date(values.consented);
            // connect to DB
            pool.getConnection(function(err, conn){
                try {
                    if(err) {
console.log('!!! getConnection failed', err);
                        if(conn)
                            conn.release();
                        res.json({ error: {
                            "code": 100,
                            "status": "Error connecting to database",
                            "err": err
                        }});
                        return;
                    }
                    if(values.responses) {
                        var results = {};
                        var lastResponse;
                        values.responses.forEach(function(r) {
                            if(r.displayedLikert) r.displayedLikert = new Date(r.displayedLikert);
                            if(r.answeredLikert) r.answeredLikert = new Date(r.answeredLikert);
                            if(r.displayedMulti) r.displayedMulti = new Date(r.displayedMulti);
                            if(r.answeredMulti) r.answeredMulti = new Date(r.answeredMulti);
                            if(!r.answeredMulti && r.answer) r.answeredMulti = new Date();
                            if(!r.answeredLikert && r.likert) r.answeredLikert = new Date();

                            conn.query(
                                "CALL sp_me(?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                                [
                                    req.query.u || req.cookies.participant,
                                    values.consented,
                                    r.question,
                                    r.answer,
                                    r.answers,
                                    r.likert,
                                    r.displayedLikert,
                                    r.answeredLikert,
                                    r.displayedMulti,
                                    r.answeredMulti,
                                    r.otherText,
                                    values.badge,
                                    values.startBadgeQuiz,
                                    values.isTest
                                ], function(err, rows) {
                                    if(err) {
console.log('!!! error in me A:', err);
// console.log('!!! values:', values);
                                        return res.json({ error: err });
                                    }
                                    var vals = processDbResults(err, rows, req.values);
                                    req.values = vals;
                                    results[r.question] = {
                                        score:      vals.user.prevScore,
                                        timeBonus:  vals.user.prevTimeBonus,
                                        dialog:     vals.user.dialog
                                    };
                                    // once final response has been recorded, return output
                                    if(Object.keys(results).length == values.responses.length) {
                                        req.values.results = results;
                                        // set cookie
                                        res.cookie('participant', req.values.user.participant);
                                        // write JSON to client
                                        return res.json(req.values);
                                    }
                                }
                            );
                        });
                    } else {
                        if(values.displayedLikert) values.displayedLikert = new Date(values.displayedLikert);
                        if(values.answeredLikert) values.answeredLikert = new Date(values.answeredLikert);
                        if(values.displayedMulti) values.displayedMulti = new Date(values.displayedMulti);
                        if(values.answeredMulti) values.answeredMulti = new Date(values.answeredMulti);
                        conn.query(
                            "CALL sp_me(?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                            [
                                req.query.u || req.cookies.participant,
                                values.consented,
                                values.question,
                                values.answer,
                                values.answers,
                                values.likert,
                                values.displayedLikert,
                                values.answeredLikert,
                                values.displayedMulti,
                                values.answeredMulti,
                                values.otherText,
                                values.badge,
                                values.startBadgeQuiz,
                                values.isTest
                            ], function(err, rows) {
                                if(err) {
console.log('!!! error in me B:', err);
// console.log('!!! values:', values);
                                    return res.json({ error: err });
                                }
                                req.values = processDbResults(err, rows, req.values);
                                // set cookie
                                res.cookie('participant', req.values.user.participant);
                                // write JSON to client
                                return res.json(req.values);
                            }
                        );
                    }
                } catch(err) {
                    return res.json({ error: err });
                } finally {
                    conn.release();
                    conn = null;
                }
            });
        },

        interactions: function(req, res) {
            var values = req.response || {};
            values = _.extend(values, req.body);
            pool.getConnection(function(err, conn){
                try {
                    if(err) {
                        if(conn)
                            conn.release();
                        res.json({
                            "code": 100,
                            "status": "Error connecting to database",
                            "err": err
                        });
                        return;
                    }
                    var insertedIds = [];
                    // iterate over interactions
// console.log('in interactions', values.interactions);
                    values.interactions.forEach(function(i){
// console.log('in interactions', i);
                        // call stored proecdure to record interaction
                        conn.query(
                            "CALL sp_interaction(?,?,?,?,?,?,?,?,?,?,?)",
                            [
                                req.query.u || req.cookies.participant,
                                i.quiz || values.quiz,
                                i.question || values.question,
                                i.answer,
                                i.video,
                                i.track,
                                i.infoText,
                                i.infoInt,
                                i.infoByte,
                                i.infoBit,
                                i.clientStamp ? new Date(i.clientStamp) : null
                            ],
                            function(err, rows) {
                                if(err) {
console.log('!!! error in interactions:', err);
// console.log('!!! values:', values);
                                    throw(err);
                                } else {
                                    insertedIds.pop(rows[0][0]);
                                }
                            }
                        );
                    });
                } catch(err) {
                    return res.json({ error: err });
                } finally {
                    conn.release();
                    conn = null;
                }
                return res.json({ interactions: insertedIds });
            });
        },

        interaction: function(req, res) {
            var values = req.response || {};
            values = _.extend(values, req.body);
            pool.getConnection(function(err, conn){
                try {
                    if(err) {
                        if(conn)
                            conn.release();
                        res.json({
                            "code": 100,
                            "status": "Error connecting to database",
                            "err": err
                        });
                        return;
                    }
                    // call stored proecdure to record interaction
                    conn.query(
                        "CALL sp_interaction(?,?,?,?,?,?,?,?,?,?,?)",
                        [
                            req.query.u || req.cookies.participant,
                            values.quiz,
                            values.question,
                            values.answer,
                            values.video,
                            values.track,
                            values.infoText,
                            values.infoInt,
                            values.infoByte,
                            values.infoBit,
                            values.clientStamp ? new Date(values.clientStamp) : null
                        ],
                        function(err, rows) {
                            if(err) {
console.log('!!! error in interaction:', err);
// console.log('!!! values:', values);
                                return res.json({ error: err });
                            }
                            // write JSON to client
                            return res.json({ interaction: rows[0][0] });
                        }
                    );
                } catch(err) {
                    return res.json({ error: err });
                } finally {
                    conn.release();
                    conn = null;
                }
            });
        },

        /**
         * Find user by id
         */
        user: function(req, res, next, id) {
            next();
        }

    };
}

