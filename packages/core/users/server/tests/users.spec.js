/* jshint -W079 */ 
/* Related to https://github.com/linnovate/mean/issues/898 */
'use strict';

const MIN_CODE = 100000;
const MAX_CODE = 999999;

const DEBUG = true;

function generateCode() {
  return Math.floor(Math.random() * (MAX_CODE - MIN_CODE + 1)) + MIN_CODE;
}

/**
 * Module dependencies.
 */

var expect = require('expect.js');

/**
 * Globals
 */
var user1, user2;

/**
 * Test Suites
 */
describe('<Unit Test>', function() {
  describe('Model User:', function() {

    before(function(done) {
      user1 = {
        username: generateCode()
      };

      user2 = {
        username: generateCode()
      };

      done();
    });

    describe('Method Save', function() {
      it('should begin without the test user', function(done) {
        // User.find({
        //   email: user1.email
        // }, function(err, users) {
        //   expect(users.length).to.equal(0);

        //   User.find({
        //     email: user2.email
        //   }, function(err, users) {
        //     expect(users.length).to.equal(0);
        //     done();
        //   });

        // });
      });

      it('should be able to save without problems', function(done) {

        // var _user = new User(user1);
        // _user.save(function(err) {
        //   expect(err).to.be(null);
        //   _user.remove();
        //   done();
        // });

      });

      // it('should check that roles are assigned and created properly', function(done) {

      //   var _user = new User(user1);
      //   _user.save(function(err) {
      //     expect(err).to.be(null);

      //     // the user1 object and users in general are created with only the 'authenticated' role
      //     expect(_user.hasRole('authenticated')).to.equal(true);
      //     expect(_user.hasRole('admin')).to.equal(false);
      //     expect(_user.isAdmin()).to.equal(false);
      //     // With the introduction of roles by circles a user has both anonymous and authenticated circles upon creation 
      //     expect(_user.roles.length).to.equal(2);
      //     _user.remove(function(err) {
      //       done();
      //     });
      //   });

      // });

      // it('should confirm that password is hashed correctly', function(done) {

      //   var _user = new User(user1);

      //   _user.save(function(err) {
      //     expect(err).to.be(null);

      //     expect(_user.hashed_password.length).to.not.equal(0);
      //     expect(_user.salt.length).to.not.equal(0);
      //     expect(_user.authenticate(user1.password)).to.equal(true);
      //     _user.remove(function(err) {
      //       done();
      //     });

      //   });
      // });

      // it('should be able to create user and save user for updates without problems', function(done) {

      //   var _user = new User(user1);
      //   _user.save(function(err) {
      //     expect(err).to.be(null);

      //     _user.name = 'Full name2';
      //     _user.save(function(err) {
      //       expect(err).to.be(null);
      //       expect(_user.name).to.equal('Full name2');
      //       _user.remove(function() {
      //         done();
      //       });
      //     });

      //   });

      // });

      it('should fail to save an existing user with the same values', function(done) {

        // var _user1 = new User(user1);
        // _user1.save();

        // var _user2 = new User(user1);

        // return _user2.save(function(err) {
        //   expect(err).to.not.be(null);
        //   _user1.remove(function() {

        //     if (!err) {
        //       _user2.remove(function() {
        //         done();
        //       });
        //     }

        //     done();

        //   });

        // });
      });

      it('should show an error when try to save without username', function(done) {

        // var _user = new User(user1);
        // _user.username = '';

        // return _user.save(function(err) {
        //   expect(err).to.not.be(null);
        //   done();
        // });
      });


      it('username should be escaped from xss', function(done) {

        // var _user = new User(user1);
        // _user.name = '<b>xss</b>';

        // return _user.save(function(err) {
        //   expect(_user.name).to.be('&lt;b&gt;xss&lt;/b&gt;');
        //   done();
        // });
      });

    });

    after(function(done) {

      /** Clean up user objects
       * un-necessary as they are cleaned up in each test but kept here
       * for educational purposes
       *
       *  var _user1 = new User(user1);
       *  var _user2 = new User(user2);
       *
       *  _user1.remove();
       *  _user2.remove();
       */

      done();
    });
  });
});
