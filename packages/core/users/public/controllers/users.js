'use strict';

angular.module('mean.system').controller('UsersController', 
  ['$scope', '$rootScope', 'MeanUser', '$state', '$mdMedia', '$window',
  function($scope, $rootScope, MeanUser, $state, $mdMedia, $window) {

    $scope.getDate = function() {
      return new Date();
    }

    var vm = this;
    vm.setValues = function() {
      // add values to scope
      vm.values = MeanUser.values || {};
      $scope.values = {
        question:  vm.values.question,
        questions: vm.values.questions,
        response:  vm.values.response,
        responses: vm.values.responses
      };
      $scope.user        = vm.values.user        || {};
      $scope.quiz        = vm.values.quiz        || false;
      $scope.videos      = vm.values.videos      || false;
      $scope.badges      = vm.values.badges      || false;
      $scope.quiz        = vm.values.quiz        || false;
      $scope.scores      = vm.values.scores      || false;
      $scope.quizScores  = vm.values.quizScores  || false;
      $scope.lastQuiz    = vm.values.lastQuiz    || false;
      $scope.leaderboard = vm.values.leaderboard || false;
      // add components of consent if not consented
      if(!vm.values || !vm.values.user || !vm.values.user.consented) {
        vm.consent = { age: false, info: false, answers: false, freely: false, rights: false };
        $scope.consent = vm.consent;
      }
    }

// Continue

    $scope.continue = function() {
      MeanUser.me();
    }

// Submit

    $scope.submitConsent = function() {
      $rootScope.$emit('submitting');
      if(vm.consent && vm.consent.age && vm.consent.info && vm.consent.answers && vm.consent.freely && vm.consent.rights) {
        // record visibility
        MeanUser.recordInteraction({
          track: 'focus',
          infoBit: !document.hidden,
          infoText: 'submitConsent'
        }, function(err, response) {
  // console.log('submitConsent visibility response', err || response);
        });
        // record consent
        MeanUser.recordConsent(function(err, response) {
          MeanUser.onIdentity(response);
        });
      }
    }

    vm.submitResponse = function() {
      $rootScope.$emit('submitting');
      // record visibility
      MeanUser.recordInteraction({
        track: 'focus',
        infoBit: !document.hidden,
        infoText: 'submitResponse'
      }, function(err, response) {
// console.log('submitResponse visibility response', err || response);
      });
      // record responses
      MeanUser.recordResponse(vm.values.response, function(err, response){
        }, true);
    }

    $scope.submitPunnett = function() {
      $rootScope.$emit('submitting');
      vm.values.responses.forEach(function(r){
        // no certainty multiplier for Punnett squares
        if(r.answer) r.likert = 1;
      });
      // record visibility
      MeanUser.recordInteraction({
        track: 'focus',
        infoBit: !document.hidden,
        infoText: 'submitPunnett'
      }, function(err, response) {
// console.log('submitPunnett visibility response', err || response);
      });
      MeanUser.recordResponses(vm.values.responses, function(err, response){
        }, true);
    }

// Likert

    $scope.submitLikert = function() {
      vm.submitResponse();
    }

    $scope.skipLikert = function() {
      vm.values.response.answeredLikert = new Date();
      vm.values.response.likert = 0;
      vm.submitResponse();
    }

// Multiple Choice

    $scope.submitMulti = function() {
      // convert object of multi multi answers to comma-separated
      // string list of answer ids before posting to API
      if(vm.values.response.answers) {
        var checked = [];
        for(var answer in vm.values.response.answers) {
          if(vm.values.response.answers[answer])
            checked.push(answer);
        }
        vm.values.response.answers = checked.join();
      }
      vm.submitResponse();
    }

    $scope.hasMultiMultiChecked = function() {
      // iterate over answers until one's value is true
      if(vm.values.response.answers)
        for(var answer in vm.values.response.answers)
          if(vm.values.response.answers[answer])
            return true;
      return false;
    }

// Badge

    $scope.submitBadge = function(badge) {
      vm.values.badge = badge;
      vm.submitResponse();
    }

    $scope.startBadgeQuiz = function() {
      vm.values.startBadgeQuiz = true;
      vm.submitResponse();
    }

// Interactions

    $scope.multiChanged = function() {
      vm.values.response.answeredMulti = new Date();
// console.log('multiChanged', vm.values.response.answer);
      MeanUser.recordInteraction({
        answer: vm.values.response.answer,
        track: 'multi',
        infoText: 'selected'
      }, function(err, response) {
// console.log('multiChanged response', err || response);
      });
    }

    $scope.multiMultiChanged = function(answer) {
// console.log('multiMultiChanged', answer, vm.values.response.answers[answer] ? 'checked' : 'unchecked');
      MeanUser.recordInteraction({
        answer: answer,
        track: 'multMult',
        infoText: vm.values.response.answers[answer] ? 'checked' : 'unchecked',
        infoBit: vm.values.response.answers[answer] ? 1 : 0
      }, function(err, response) {
// console.log('multiMultiChanged response', err || response);
      });
    }

    $scope.otherChanged = function(answer) {
// console.log('otherChanged', answer, vm.values.response.otherText);
      MeanUser.recordInteraction({
        answer: answer,
        track: 'other',
        infoText: vm.values.response.otherText
      }, function(err, response) {
// console.log('otherChanged response', err || response);
      });
    }

    $scope.likertChanged = function() {
// console.log('likertChanged', vm.values.response.likert);
      vm.values.response.answeredLikert = new Date();
      MeanUser.recordInteraction({
        track: 'likert',
        infoByte: vm.values.response.likert
      }, function(err, response) {
// console.log('likertChanged response', err || response);
      });
    }

    $scope.dropDownChanged = function(response) {
// console.log('dropDownChanged', response.answer);
      response.answeredMulti = new Date();
      MeanUser.recordInteraction({
        track: 'dropDown',
        infoInt: response.answer
      }, function(err, response) {
// console.log('dropDownChanged response', err || response);
      });
    }

    // focus/blur tracking
    window.onBlur = function() {
      if(!document.addEventListener)
        document.hidden = false;
      MeanUser.recordInteraction({
        track: 'focus',
        infoBit: 0,
        infoText: 'onBlur'
      }, function(err, response) {
// console.log('onBlur response', err || response);
      });
    };
    window.onFocus = function() {
      if(!document.addEventListener)
        document.hidden = true;
      MeanUser.recordInteraction({
        track: 'focus',
        infoBit: 1,
        infoText: 'onFocus'
      }, function(err, response) {
// console.log('onFocus response', err || response);
      });
    };
    window.onVisibilityChanged = function() {
      MeanUser.recordInteraction({
        track: 'focus',
        infoBit: !document.hidden,
        infoText: 'visibilitychange: ' + (document.hidden ? 'hidden' : 'visible')
      }, function(err, response) {
// console.log('onVisibilityChanged response', document.hidden ? 'hidden' : 'visible', err || response);
      });
    }
    if(document.addEventListener && !window.hookedVisibility) {
      document.addEventListener("visibilitychange", onVisibilityChanged);
      window.hookedVisibility = true;
    }
    if (/*@cc_on!@*/false) { // check for Internet Explorer
      document.onfocusin = onFocus;
      document.onfocusout = onBlur;
    } else {
      window.onfocus = onFocus;
      window.onblur = onBlur;
    }

// Videos

    $scope.videoPreviewClicked = function($event) {
      // get the wrapper of the clicked video
      var $wrapper = $($event.currentTarget);
      // get the clicked image and parse values
      var $img = $wrapper.find('img[data-youtube-preview]');
      var youtube = $img.attr('data-youtube-preview');
      var video = $img.attr('data-video');
      // remove the image, swap the wrapper class, and inject the player
      $wrapper
        .empty()
        .attr('class','embed-responsive embed-responsive-16by9')
        .append($('<div id="video-' + youtube + '" data-youtube="' + youtube + '" data-video="' + video + '" class="embed-responsive-item" data-autoplay="true"><center><br/><br/><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><br/>Loading...</center></div>'));
    }

// Leaderboard

    $scope.isArray = function(obj) {
      return Array.isArray(obj);
    }

    $scope.getBonus = function(rank) {
      if(rank ==  1) return '$6';
      if(rank <=  3) return '$4';
      if(rank <=  7) return '$2';
      if(rank <= 13) return '$1';
      return '';
    }

// Initialize

    // set values on init
    vm.setValues();
    // update values on loggedin
    $rootScope.$on('loggedin', function() {
      vm.setValues();
      // scroll to top of page
      $window.scrollTo(0,0);
      // set title
      if(vm.values && vm.values.user && vm.values.user.participant)
        document.title = (vm.values.user.isTest ? 'TEST ' : '') + 
          "Participant: " + vm.values.user.participant;
      // record window state
      MeanUser.recordInteraction({
        track: 'focus',
        infoBit: !document.hidden,
        infoText: 'onloggedin: ' + (document.hidden ? 'hidden' : 'visible')
      }, function(err, response) {
// console.log('onloggedin focus response', document.hidden ? 'hidden' : 'visible', err || response);
      });
    });
  }
]);
