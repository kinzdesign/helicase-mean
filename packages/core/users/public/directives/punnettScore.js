'use strict';

angular.module('mean.system').directive('punnettScore', function() { 
  return { 
    restrict: 'E',
    scope: {
      response: '=response'
    }, 
    templateUrl: '/users/directives/punnettScore.html' 
  }; 
});