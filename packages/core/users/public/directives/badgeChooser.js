'use strict';

angular.module('mean.system').directive('badgeChooser', function() { 
  return { 
    restrict: 'E',
    templateUrl: '/users/directives/badgeChooser.html' 
  }; 
});