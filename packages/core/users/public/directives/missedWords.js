'use strict';

angular.module('mean.system').directive('missedWords', function() { 
  return { 
    restrict: 'E',
    templateUrl: '/users/directives/missedWords.html' 
  }; 
});