'use strict';

angular.module('mean.system').directive('lastQuiz', function() { 
  return { 
    restrict: 'E', 
    templateUrl: '/users/directives/lastQuiz.html' 
  }; 
});