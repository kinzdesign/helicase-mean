'use strict';

angular.module('mean.system').directive('likertScale', function() { 
  return { 
    restrict: 'E', 
    templateUrl: '/users/directives/likertScale.html' 
  }; 
});