'use strict';

angular.module('mean.system').directive('videoList', function() { 
  return { 
    restrict: 'E',
    templateUrl: '/users/directives/videoList.html' 
  }; 
});