'use strict';

angular.module('mean.system').directive('clocks', function() { 
  return { 
    restrict: 'E',
    templateUrl: '/users/directives/clocks.html' 
  }; 
});