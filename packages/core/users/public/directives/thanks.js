'use strict';

angular.module('mean.system').directive('thanks', function() { 
  return { 
    restrict: 'E',
    templateUrl: '/users/directives/thanks.html' 
  }; 
});