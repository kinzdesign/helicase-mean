'use strict';

angular.module('mean.system').directive('continue', function() { 
  return { 
    restrict: 'E',
    templateUrl: '/users/directives/continue.html' 
  }; 
});