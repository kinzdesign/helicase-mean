'use strict';

angular.module('mean.system').directive('multipleChoice', function() { 
  return { 
    restrict: 'E', 
    templateUrl: '/users/directives/multipleChoice.html' 
  }; 
});