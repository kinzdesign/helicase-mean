'use strict';

angular.module('mean.system').directive('dropDown', function() { 
  return { 
    restrict: 'E',
    scope: {
      question: '=question',
      response: '=response'
    }, 
    templateUrl: '/users/directives/dropDown.html' 
  }; 
});