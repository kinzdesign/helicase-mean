'use strict';

angular.module('mean.system').directive('quizScores', function() { 
  return { 
    restrict: 'E', 
    templateUrl: '/users/directives/quizScores.html' 
  }; 
});