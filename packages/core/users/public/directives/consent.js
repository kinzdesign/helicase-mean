'use strict';

angular.module('mean.system').directive('consent', function() { 
  return { 
    restrict: 'E', 
    templateUrl: '/users/directives/consent.html' 
  }; 
});