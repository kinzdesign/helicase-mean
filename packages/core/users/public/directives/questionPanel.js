'use strict';

angular.module('mean.system').directive('questionPanel', function() { 
  return { 
    restrict: 'E', 
    templateUrl: '/users/directives/questionPanel.html' 
  }; 
});