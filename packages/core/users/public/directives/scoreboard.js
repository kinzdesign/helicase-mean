'use strict';

angular.module('mean.system').directive('scoreboard', function() { 
  return { 
    restrict: 'E',
    templateUrl: '/users/directives/scoreboard.html' 
  }; 
});