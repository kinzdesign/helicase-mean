'use strict';

angular.module('mean.system').directive('expired', function() { 
  return { 
    restrict: 'E', 
    templateUrl: '/users/directives/expired.html' 
  }; 
});