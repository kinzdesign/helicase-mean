'use strict';

angular.module('mean.system').directive('questionScore', function() { 
  return { 
    restrict: 'E',
    templateUrl: '/users/directives/questionScore.html' 
  }; 
});