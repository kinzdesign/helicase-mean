'use strict';
var $$interval;

angular.module('mean.users').factory('MeanUser', [ '$rootScope', '$http', '$location', '$stateParams', '$cookies', '$q', '$timeout', '$document', '$compile', '$interval',
  function($rootScope, $http, $location, $stateParams, $cookies, $q, $timeout, $document, $compile, $interval) {
    $$interval = $interval;

    var self;

    function MeanUserKlass(){
      this.aclDefer = $q.defer();
      this.name = 'users';
      this.user = { participant: $location.search().u };
      this.acl = this.aclDefer.promise;
      self = this;
      this.me();
      this.acl.then(function(response) {
        self.acl = response;
        delete self.aclDefer;
      });
    }

    function prettifyList(strList) {
      if(strList) {
        strList = strList.split(',');
        // single-item lists
        if(strList.length == 1)
          return strList[0];
        // two-item lists
        if(strList.length == 2)
          return strList[0] + ' and ' + strList[1];
        // larger lists
        if(strList.length > 2) {
          // add spaces after commas
          strList = strList.join(', ');
          // find last space
          var lastSpace = strList.lastIndexOf(' ');
          // inject 'and'
          return strList.substring(0,lastSpace) + ' and' + strList.substring(lastSpace);
        }
      }
      return false;
    }

    MeanUserKlass.prototype.me = function() {
      $http.get('/api/me' + (this.user.participant ? ('?u=' + this.user.participant) : '')).success(function(response) {
        if(response.user) {
          response.user.experience = response.user.experience || response.user.exp;
          response.user.exp = response.user.exp || response.user.experience;
        }
        self.onIdentity.bind(self)(response);
      });
    }

    MeanUserKlass.prototype.prepQuestionAndResponse = function(question, response) {
      // if this is the first time showing likert
      if(question.likertIntroHtml && !response.displayedLikert) {
        // set date likert first displayed
        response.displayedLikert = new Date();
        // POST displayed date to server without reloading identity
        self.recordResponse(response, function(err, res) {
// console.log('recorded displayedLikert', err || res);
        }, false);
      }
      // if this is the first time showing multiple choice
      if(question.multiIntroHtml && !response.displayedMulti) {
        // set date multi first displayed
        response.displayedMulti = new Date();
        // POST displayed date to server without reloading identity
        self.recordResponse(response, function(err, res) {
// console.log('recorded displayedMulti', err || res);
        }, false);
      }
      if(question.allowMultipleMulti == 1)
        response.answers = {};
    }

    MeanUserKlass.prototype.onIdentity = function(response) {
      var self = this;
      var user, destination, values;
      destination = angular.isDefined(response.redirect) ? response.redirect : destination;
      $cookies.remove('redirect');
      this.values = response || {};
      if(this && this.values) {
        if(this.values.user) {
          $cookies.put('participant', this.values.user.participant);
          if(this.values.user.missedWords) {
            this.values.user.multipleMissedWords = this.values.user.missedWords.indexOf(',') > 0;
            this.values.user.missedWords = prettifyList(this.values.user.missedWords);
          }
          // add participant url
          if(this.values.user && this.values.user.participant) {
            var port = $location.port();
            this.values.user.participantUrl = 
              $location.protocol() + '://' +
              $location.host() + 
              (port && port != 80 && port != 443 ? (':' + port) : '') +
              '/?u=' +
              this.values.user.participant;
// console.log('participantUrl', this.values.user.participantUrl);
          }

        }
        if(this.values.question) {
          // ensure response exists
          this.values.response = this.values.response || {};
          // prepare question and response
          this.prepQuestionAndResponse(this.values.question, this.values.response);
        }
        if(this.values.questions) {
          // ensure response array exists
          this.values.responses = this.values.responses || new Array(this.values.questions.length);
          for(var i = 0; i < this.values.questions.length; ++i) {
            // ensure response exists
            this.values.responses[i] = this.values.responses[i] || {};
            // prepare question and response
            this.prepQuestionAndResponse(this.values.questions[i], this.values.responses[i]);
          }
        }
        // handle gamification
        if(this.values.user && (this.values.user.exp == 'G' || this.values.user.experience == 'G')) {
          var resp = this.getResponse();
          if(resp && resp.displayedMulti && !resp.submittedMulti) {
            // set clock timer if multiple choice question hasn't been answered
            $$interval(this.updateClock, 100);
            if(this.values.question) 
              this.values.question.displayedTime = 0;
          }
        }
      }
      this.user = this.values.user;
      // set query string
      if(this.values.user && this.values.user.participant)
        $location.search('u', this.values.user.participant);
// console.log('onIdentity', this.values);
      $rootScope.$emit('loggedin');
    };

    MeanUserKlass.prototype.updateClock = function() {
      if(
        self.values.response && 
        self.values.response.displayedMulti && 
        !self.values.response.submittedMulti &&
        self.values.question
      ) {
        self.values.question.displayedTime = new Date() - new Date(self.values.response.displayedMulti);
      }
    }

    MeanUserKlass.prototype.getResponse = function() {
      if(this.values) {
        if(this.values.response)
          return this.values.response;
        if(this.values.responses)
          return this.values.responses[0];
      }
      return false;
    }

    MeanUserKlass.prototype.onIdFail = function (response) {
      $cookies.remove('participant');
      $location.path(response.redirect);
    };

    MeanUserKlass.prototype.recordConsent = function (next) {
// console.log('recordConsent: this.consent', this.consent);
      $http.post('/api/respond?u=' + this.user.participant, {
        username: this.user.participant,
        consented: new Date(),
        isTest: $location.search().test ? true : false
      }).success(function(response){
        self.onIdentity(response);
        return next(false, response);
      }).error(function(response){
        return next(response);
      });
    };

    MeanUserKlass.prototype.recordResponse = function (response, next, callOnIdentity) {
// console.log('recordResponse: this.values, response', this.values, response);
      response = response || {};
      // if single badge in array, pass its id as badge parameter
      if(!this.values.badge && this.values.badges && this.values.badges.length == 1)
        this.values.badge = this.values.badges[0].badge;
      $http.post('/api/respond?u=' + this.user.participant, {
        participant: this.user.participant,
        question: response.question,
        answer: response.answer,
        answers: response.answers,
        likert: response.likert,
        displayedLikert: response.displayedLikert,
        answeredLikert: response.answeredLikert,
        displayedMulti: response.displayedMulti,
        answeredMulti: response.answeredMulti,
        badge: this.values.badge || this.values.user.badge,
        startBadgeQuiz: this.values.startBadgeQuiz,
        responses: response.responses,
        otherText: response.otherText,
        isTest: $location.search().test ? true : false
      }).success(function(response){
        if(callOnIdentity)
          self.onIdentity(response);
        return next(false, response);
      }).error(function(response){
        return next(response);
      });
    };

    MeanUserKlass.prototype.recordResponses = function (responses, next) {
// console.log('recordResponses: this.values, responses', this.values, responses);
      responses = responses || {};
      $http.post('/api/respond?u=' + this.user.participant, {
        participant: this.user.participant,
        responses: responses,
        isTest: $location.search().test ? true : false
      }).success(function(response){
        self.onIdentity(response);
        return next(false, response);
      }).error(function(response){
        return next(response);
      });
    };

    MeanUserKlass.prototype.recordInteractions = function (interactions, next) {
// console.log('recordInteractions: this.values, interactions', this.values, interactions);
      // only post if there are interactions in the queue
      if(interactions && interactions.length > 0) {
        $http.post('/api/interactions?u=' + this.user.participant, {
          username: this.user.participant,
          interactions: interactions,
          quiz: this.user.quiz || this.user.badge,
          question: this.user.question
        }).success(function(response) {
          return next(false, response);
        }).error(function(response) {
          return next(response);
        });
      }
    };

    MeanUserKlass.prototype.recordInteraction = function (interaction, next) {
// console.log('recordInteraction: this.values, interaction', this.values, interaction);
      // copy participant data and current state to the interaction
      interaction.participant = this.values.user.participant;
      interaction.quiz = interaction.quiz || this.values.user.quiz;
      interaction.question = interaction.question || this.values.user.question;
      interaction.clientStamp = interaction.clientStamp || new Date();
      if(!interaction.answer && this.values.response && this.values.response.answer)
        interaction.answer = this.values.response.answer;
      // post the interaction to the server
      $http.post('/api/interaction?u=' + this.user.participant, 
        interaction
      ).success(function(response) {
        return next(false, response);
      }).error(function(err) {
        return next(err);
      });
    };

    MeanUserKlass.prototype.currentParticipantID = function () {
      return this.user.username || $cookies.get('participant');
    }

    var MeanUser = new MeanUserKlass();

    return MeanUser;
  }
]);
