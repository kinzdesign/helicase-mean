#!/bin/bash
mysql \
	-h $DEV_HOST \
	-P $DEV_PORT \
	-u $DEV_MASTER_USER \
	-p$DEV_MASTER_PASS \
	$DEV_DATABASE