#!/bin/bash
echo
echo 'Restoring helicase database structure to production...'
mysql -h $PROD_HOST \
	-P $PROD_PORT \
	-u $PROD_MASTER_USER \
	-p$PROD_MASTER_PASS \
	--comments $PROD_DATABASE \
< sql/prod.structure.sql
echo 'Done!'
echo
echo 'Restoring helicase seed data to production...'
mysql -h $PROD_HOST \
	-P $PROD_PORT \
	-u $PROD_MASTER_USER \
	-p$PROD_MASTER_PASS \
	--comments $PROD_DATABASE \
< sql/prod.seeds.sql
echo 'Done!'
echo
echo 'Restoring helicase participant data to production...'
mysql -h $PROD_HOST \
	-P $PROD_PORT \
	-u $PROD_MASTER_USER \
	-p$PROD_MASTER_PASS \
	--comments $PROD_DATABASE \
< sql/prod.data.sql
echo 'Done!'
echo