-- MySQL dump 10.13  Distrib 5.7.14, for Linux (x86_64)
--
-- ------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answers` (
  `answer` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `question` smallint(5) unsigned NOT NULL,
  `answerText` varchar(255) NOT NULL,
  `score` tinyint(4) NOT NULL DEFAULT '0',
  `isOther` bit(1) NOT NULL DEFAULT b'0',
  `displayOrder` tinyint(3) unsigned DEFAULT NULL,
  `answerGroup` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`answer`),
  KEY `fk_answers_1_idx` (`question`),
  CONSTRAINT `fk_answers_1` FOREIGN KEY (`question`) REFERENCES `questions` (`question`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `badges`
--

DROP TABLE IF EXISTS `badges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `badges` (
  `badge` varchar(3) NOT NULL,
  `pathway` char(2) NOT NULL,
  `badgeName` varchar(45) NOT NULL,
  `displayOrder` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`badge`),
  KEY `badges_pathway_idx` (`pathway`),
  CONSTRAINT `fk_badges_1` FOREIGN KEY (`pathway`) REFERENCES `pathways` (`pathway`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `experiences`
--

DROP TABLE IF EXISTS `experiences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experiences` (
  `experience` char(1) NOT NULL,
  `experienceName` varchar(45) NOT NULL,
  PRIMARY KEY (`experience`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `interaction_times`
--

DROP TABLE IF EXISTS `interaction_times`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interaction_times` (
  `analysis_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `participant` mediumint(8) unsigned NOT NULL,
  `quiz` varchar(6) DEFAULT NULL,
  `question` smallint(5) unsigned DEFAULT NULL,
  `video` tinyint(3) unsigned DEFAULT NULL,
  `track` varchar(8) DEFAULT NULL,
  `startId` bigint(20) unsigned NOT NULL,
  `endId` bigint(20) unsigned NOT NULL,
  `startTime` datetime(3) NOT NULL,
  `endTime` datetime(3) NOT NULL,
  PRIMARY KEY (`analysis_id`),
  KEY `fk_interaction_times_participant_idx` (`participant`),
  CONSTRAINT `fk_interaction_times_participant` FOREIGN KEY (`participant`) REFERENCES `participants` (`participant`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `interactions`
--

DROP TABLE IF EXISTS `interactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interactions` (
  `interaction` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `participant` mediumint(8) unsigned NOT NULL,
  `quiz` varchar(6) DEFAULT NULL,
  `question` smallint(5) unsigned DEFAULT NULL,
  `answer` smallint(5) unsigned DEFAULT NULL,
  `video` tinyint(3) unsigned DEFAULT NULL,
  `track` varchar(8) DEFAULT NULL,
  `infoText` varchar(160) DEFAULT NULL,
  `infoInt` int(11) DEFAULT NULL,
  `infoByte` tinyint(4) DEFAULT NULL,
  `infoBit` bit(1) DEFAULT NULL,
  `clientStamp` datetime(3) DEFAULT NULL,
  `serverStamp` datetime(3) NOT NULL,
  PRIMARY KEY (`interaction`),
  KEY `fk_interactions_question_idx` (`question`),
  KEY `fk_interactions_answer_idx` (`answer`),
  KEY `fk_interactions_video_idx` (`video`),
  KEY `fk_interactions_participant_idx` (`participant`),
  KEY `fk_interactions_quiz_idx` (`quiz`),
  CONSTRAINT `fk_interactions_answer` FOREIGN KEY (`answer`) REFERENCES `answers` (`answer`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_interactions_participant` FOREIGN KEY (`participant`) REFERENCES `participants` (`participant`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_interactions_question` FOREIGN KEY (`question`) REFERENCES `questions` (`question`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_interactions_quiz` FOREIGN KEY (`quiz`) REFERENCES `quizzes` (`quiz`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_interactions_video` FOREIGN KEY (`video`) REFERENCES `videos` (`video`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `participants`
--

DROP TABLE IF EXISTS `participants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participants` (
  `participant` mediumint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `consented` datetime(3) DEFAULT NULL,
  `finishedStamp` datetime(3) DEFAULT NULL,
  `pathway` char(2) DEFAULT NULL,
  `experience` char(1) CHARACTER SET big5 DEFAULT NULL,
  `vignette` varchar(6) DEFAULT NULL,
  `basePaidAt` datetime(3) DEFAULT NULL,
  `bonusPaidAt` datetime(3) DEFAULT NULL,
  `isTest` bit(1) NOT NULL DEFAULT b'0',
  `worker` char(14) DEFAULT NULL,
  `assignment` char(30) DEFAULT NULL,
  `hit` char(30) DEFAULT NULL,
  `bonusAmount` smallint(5) unsigned DEFAULT NULL,
  `rankInCohort` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`participant`),
  UNIQUE KEY `worker_UNIQUE` (`worker`),
  KEY `idx_participants_pathway` (`pathway`),
  KEY `idx_participants_experience` (`experience`),
  KEY `fk_participants_vignette_idx` (`vignette`),
  CONSTRAINT `fk_participants_pathway` FOREIGN KEY (`pathway`) REFERENCES `pathways` (`pathway`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_participants_vignette` FOREIGN KEY (`vignette`) REFERENCES `quizzes` (`quiz`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pathways`
--

DROP TABLE IF EXISTS `pathways`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pathways` (
  `pathway` char(2) NOT NULL,
  `pathwayName` varchar(45) NOT NULL,
  PRIMARY KEY (`pathway`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `q_participant_quiz`
--

DROP TABLE IF EXISTS `q_participant_quiz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `q_participant_quiz` (
  `q_id` int(11) NOT NULL AUTO_INCREMENT,
  `participant` mediumint(6) unsigned NOT NULL,
  `quiz` varchar(6) NOT NULL,
  PRIMARY KEY (`q_id`),
  UNIQUE KEY `uq_q_participant_quiz` (`participant`,`quiz`),
  KEY `fk_q_participant_quiz_participant_idx` (`participant`),
  KEY `fk_q_participant_quiz_quiz_idx` (`quiz`),
  CONSTRAINT `fk_q_participant_quiz_participant` FOREIGN KEY (`participant`) REFERENCES `participants` (`participant`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_q_participant_quiz_quiz` FOREIGN KEY (`quiz`) REFERENCES `quizzes` (`quiz`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `question` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `quiz` varchar(6) NOT NULL,
  `questionGroup` tinyint(3) unsigned DEFAULT NULL,
  `displayOrder` tinyint(3) unsigned NOT NULL,
  `introHtml` text,
  `outroHtml` text,
  `likertIntroHtml` varchar(1000) DEFAULT NULL,
  `multiIntroHtml` varchar(1000) DEFAULT NULL,
  `header` varchar(30) DEFAULT NULL,
  `questionFormat` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `likertMinLabel` varchar(45) DEFAULT 'Strongly Disagree',
  `likertMaxLabel` varchar(45) DEFAULT 'Strongly Agree',
  PRIMARY KEY (`question`),
  KEY `idx_questions_quiz` (`quiz`),
  CONSTRAINT `fk_questions_quizzes` FOREIGN KEY (`quiz`) REFERENCES `quizzes` (`quiz`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quizzes`
--

DROP TABLE IF EXISTS `quizzes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quizzes` (
  `quiz` varchar(6) NOT NULL,
  `title` varchar(45) NOT NULL,
  `note` varchar(45) DEFAULT NULL,
  `introHtml` varchar(1000) DEFAULT NULL,
  `quizFormat` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `displayOrder` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`quiz`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `responses`
--

DROP TABLE IF EXISTS `responses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `responses` (
  `responseId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `participant` mediumint(6) unsigned NOT NULL,
  `question` smallint(5) unsigned NOT NULL,
  `answer` smallint(5) unsigned DEFAULT NULL,
  `likert` tinyint(1) unsigned DEFAULT NULL,
  `sentMulti` datetime(3) DEFAULT NULL,
  `displayedMulti` datetime(3) DEFAULT NULL,
  `answeredMulti` datetime(3) DEFAULT NULL,
  `submittedMulti` datetime(3) DEFAULT NULL,
  `sentLikert` datetime(3) DEFAULT NULL,
  `displayedLikert` datetime(3) DEFAULT NULL,
  `answeredLikert` datetime(3) DEFAULT NULL,
  `submittedLikert` datetime(3) DEFAULT NULL,
  `score` tinyint(3) DEFAULT NULL,
  `timeBonus` tinyint(2) unsigned DEFAULT NULL,
  `otherText` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`responseId`),
  UNIQUE KEY `uq_responses_participant_question` (`participant`,`question`),
  KEY `fk_responses_question_idx` (`question`),
  KEY `fk_responses_answer_idx` (`question`,`answer`),
  CONSTRAINT `fk_responses_answer` FOREIGN KEY (`question`, `answer`) REFERENCES `answers` (`question`, `answer`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_responses_participant` FOREIGN KEY (`participant`) REFERENCES `participants` (`participant`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_responses_question` FOREIGN KEY (`question`) REFERENCES `questions` (`question`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `responses_multi`
--

DROP TABLE IF EXISTS `responses_multi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `responses_multi` (
  `participant` mediumint(8) unsigned NOT NULL,
  `question` smallint(5) unsigned NOT NULL,
  `answer` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`participant`,`question`,`answer`),
  KEY `fk_responses_multi_question_idx` (`question`),
  KEY `fk_responses_multi_answer_idx` (`question`,`answer`),
  CONSTRAINT `fk_responses_multi_answer` FOREIGN KEY (`question`, `answer`) REFERENCES `answers` (`question`, `answer`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_responses_multi_participant` FOREIGN KEY (`participant`) REFERENCES `participants` (`participant`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_responses_multi_question` FOREIGN KEY (`question`) REFERENCES `questions` (`question`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos` (
  `video` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `youtube` varchar(12) NOT NULL,
  `badge` varchar(3) NOT NULL,
  `title` varchar(75) NOT NULL,
  `user` varchar(19) NOT NULL,
  `displayOrder` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`video`),
  UNIQUE KEY `youtube_UNIQUE` (`youtube`),
  KEY `fk_videos_badge_idx` (`badge`),
  CONSTRAINT `fk_videos_badge` FOREIGN KEY (`badge`) REFERENCES `badges` (`badge`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `vw_interaction_time_summary`
--

DROP TABLE IF EXISTS `vw_interaction_time_summary`;
/*!50001 DROP VIEW IF EXISTS `vw_interaction_time_summary`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_interaction_time_summary` AS SELECT 
 1 AS `participant`,
 1 AS `focusTime`,
 1 AS `videoTime`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_participant_score`
--

DROP TABLE IF EXISTS `vw_participant_score`;
/*!50001 DROP VIEW IF EXISTS `vw_participant_score`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_participant_score` AS SELECT 
 1 AS `participant`,
 1 AS `score`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_question_format`
--

DROP TABLE IF EXISTS `vw_question_format`;
/*!50001 DROP VIEW IF EXISTS `vw_question_format`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_question_format` AS SELECT 
 1 AS `question`,
 1 AS `showLikert`,
 1 AS `showMulti`,
 1 AS `renderLikertBefore`,
 1 AS `allowMultipleMulti`,
 1 AS `isPunnett`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_question_status`
--

DROP TABLE IF EXISTS `vw_question_status`;
/*!50001 DROP VIEW IF EXISTS `vw_question_status`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_question_status` AS SELECT 
 1 AS `participant`,
 1 AS `quiz`,
 1 AS `displayOrder`,
 1 AS `question`,
 1 AS `questionGroup`,
 1 AS `completed`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_quiz_format`
--

DROP TABLE IF EXISTS `vw_quiz_format`;
/*!50001 DROP VIEW IF EXISTS `vw_quiz_format`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_quiz_format` AS SELECT 
 1 AS `quiz`,
 1 AS `quizFormat`,
 1 AS `canGamify`,
 1 AS `showHeader`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_quiz_score`
--

DROP TABLE IF EXISTS `vw_quiz_score`;
/*!50001 DROP VIEW IF EXISTS `vw_quiz_score`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_quiz_score` AS SELECT 
 1 AS `participant`,
 1 AS `quiz`,
 1 AS `nLikert`,
 1 AS `cLikert`,
 1 AS `nMulti`,
 1 AS `cMulti`,
 1 AS `nCorrect`,
 1 AS `completed`,
 1 AS `multiScore`,
 1 AS `timeBonuses`,
 1 AS `totalScore`,
 1 AS `isScored`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_quiz_status`
--

DROP TABLE IF EXISTS `vw_quiz_status`;
/*!50001 DROP VIEW IF EXISTS `vw_quiz_status`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_quiz_status` AS SELECT 
 1 AS `q_id`,
 1 AS `participant`,
 1 AS `quiz`,
 1 AS `completed`*/;
SET character_set_client = @saved_cs_client;

--
--
/*!50003 DROP FUNCTION IF EXISTS `fn_assign_vignette` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE FUNCTION `fn_assign_vignette`(
  p_participant MEDIUMINT UNSIGNED, 
  p_pathway     CHAR(2)
) RETURNS varchar(6) CHARSET utf8
BEGIN
  DECLARE p_vignette VARCHAR(6);
  DECLARE p_genome1, p_genome2 SMALLINT UNSIGNED;
    
  SELECT  vignette
  INTO    p_vignette
  FROM    participants
  WHERE   participant = p_participant;
    
    IF p_vignette IS NULL THEN

    CASE p_pathway
      WHEN 'CF' THEN
        -- choose randomly from 3 options
        CASE fn_random(0, 2)
          WHEN 0 THEN
            SET p_vignette  = 'V-CF-X';
            SET p_genome1   = 8;  -- carrier    XX
            SET p_genome2   = 3046; -- non-carrier  XY
          WHEN 1 THEN
            SET p_vignette  = 'V-CF-Y';
            SET p_genome1   = 3326; -- non-carrier  XX
            SET p_genome2   = 216;  -- carrier    XY
          WHEN 2 THEN
            SET p_vignette  = 'V-CF-2';
            SET p_genome1   = 8;  -- carrier    XX
            SET p_genome2   = 1265; -- carrier    XY
        END CASE;
      WHEN 'CH' THEN
        -- choose randomly from 3 options
        CASE fn_random(0, 2)
          WHEN 0 THEN
            SET p_vignette  = 'V-CH-0';
            SET p_genome1   = 3046; -- low
          WHEN 1 THEN
            SET p_vignette  = 'V-CH-1';
            SET p_genome1   = 216;  -- mid
          WHEN 2 THEN
            SET p_vignette  = 'V-CH-2';
            SET p_genome1   = 1265; -- high
        END CASE;
    END CASE;
    
    -- record vignette and genomes in DB
    UPDATE  participants
    SET     vignette  = p_vignette,
            genome1   = p_genome1,
            genome2   = p_genome2
    WHERE   participant = p_participant;
        
  END IF;
    
  RETURN  p_vignette;
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fn_enqueue_quiz` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE FUNCTION `fn_enqueue_quiz`(
  p_participant MEDIUMINT(6) UNSIGNED,
  p_quiz        VARCHAR(6)
) RETURNS varchar(6) CHARSET utf8
BEGIN
  -- make sure this quiz isn't already in participant's queue
  IF NOT EXISTS (SELECT * FROM q_participant_quiz WHERE participant = p_participant AND quiz = p_quiz) THEN
    -- equeue quiz
    INSERT
    INTO  q_participant_quiz (participant, quiz)
    VALUES  (p_participant, p_quiz);
  END IF;

  RETURN p_quiz;
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fn_first_incomplete_quiz` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE FUNCTION `fn_first_incomplete_quiz`(
  p_participant MEDIUMINT(6) UNSIGNED
) RETURNS varchar(6) CHARSET utf8
BEGIN
  DECLARE p_quiz  VARCHAR(6);

  SELECT  quiz
  INTO    p_quiz
  FROM    vw_quiz_status
  WHERE   participant = p_participant AND completed = 0
  ORDER   BY q_id
  LIMIT   1;

  RETURN  COALESCE(p_quiz, '');
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fn_get_answers_for_question` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE FUNCTION `fn_get_answers_for_question`(
  p_participant MEDIUMINT(6) UNSIGNED,
  p_question    SMALLINT UNSIGNED
) RETURNS varchar(21844) CHARSET utf8
BEGIN

RETURN  (
  SELECT  CASE WHEN r.otherText IS NULL 
            THEN  m.answers
            ELSE  CONCAT(m.answers, '|[other: ', r.otherText,']')
          END
  FROM    responses AS r
          LEFT JOIN (
            SELECT  r1.participant, r1.question, GROUP_CONCAT(a1.answerText SEPARATOR '|') AS answers
            FROM    responses_multi AS r1
                    INNER JOIN answers AS a1 ON r1.answer = a1.answer
            GROUP   BY r1.participant, r1.question
          ) AS m ON r.participant = m.participant AND r.question = m.question
  WHERE   r.participant = p_participant
          AND r.question = p_question
);
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fn_get_answer_for_question` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE FUNCTION `fn_get_answer_for_question`(
  p_participant MEDIUMINT(6) UNSIGNED,
  p_question    SMALLINT UNSIGNED
) RETURNS varchar(255) CHARSET utf8
BEGIN

RETURN  (
  SELECT  CASE WHEN r.otherText IS NULL 
            THEN  a.answerText
            ELSE  CONCAT(a.answerText, ' (', r.otherText, ')')
          END
  FROM    answers AS a
          INNER JOIN responses AS r ON a.answer = r.answer
  WHERE   r.participant = p_participant
          AND r.question = p_question
);
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fn_get_badge` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE FUNCTION `fn_get_badge`(
  p_badge       VARCHAR(3),
  p_quiz        VARCHAR(6),
  p_participant   MEDIUMINT(6) UNSIGNED
) RETURNS varchar(3) CHARSET utf8
BEGIN
  IF p_badge IS NULL THEN 
    -- set badge to current quiz if it's a badge quiz and 
    -- participant has not completed quiz
    SELECT  badge
    INTO    p_badge
    FROM    badges
            LEFT JOIN vw_quiz_status ON badge = quiz
    WHERE   badge = p_quiz 
            AND participant = p_participant
            AND completed = 0;
  ELSEIF EXISTS (
    SELECT  *
    FROM    vw_quiz_status
    WHERE   participant = p_participant AND quiz = p_badge AND completed = 1
  ) THEN
    RETURN NULL;
  END IF;

  RETURN p_badge;
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fn_get_experience` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE FUNCTION `fn_get_experience`(
  p_pathway     CHAR(2)
) RETURNS char(1) CHARSET utf8
BEGIN
  DECLARE n_diff TINYINT;

  -- get finished, non-test experience counts for selected pathway
  SELECT  SUM(CASE WHEN experience = 'G' THEN 1 ELSE 0 END) -
          SUM(CASE WHEN experience = 'C' THEN 1 ELSE 0 END)
  INTO    n_diff
  FROM    participants
  WHERE   pathway = p_pathway 
          AND finishedStamp IS NOT NULL 
          AND isTest = 0;

  -- compare counts to assign experience
  CASE 
    WHEN n_diff < 0 THEN
      -- excess control, add gamified
      RETURN 'G';
    WHEN n_diff > 0 THEN
      -- excess gamified, add control
      RETURN 'C';
    ELSE
      -- even counts, randomize (coin flip)
      RETURN
        CASE WHEN fn_random(0, 1) = 1 
          THEN 'G'
          ELSE 'C' 
        END;
  END CASE;
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fn_get_missed_words` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE FUNCTION `fn_get_missed_words`(
  p_participant   MEDIUMINT(6) UNSIGNED
) RETURNS varchar(255) CHARSET utf8
BEGIN
  IF  ( -- participant is gamified
        SELECT  experience
        FROM    participants
        WHERE   participant = p_participant
      ) = 'G'
      AND
      ( -- hasn't started PGEN1
        SELECT  COUNT(*) 
        FROM    q_participant_quiz
        WHERE   participant = p_participant 
      ) = 1
      AND
      ( -- has answered all of FCGT
        SELECT  COUNT(*) 
        FROM    responses 
        WHERE   participant = p_participant AND score IS NOT NULL
      ) = 8 
  THEN
    RETURN COALESCE((
      SELECT  LOWER(GROUP_CONCAT(q.header SEPARATOR ','))
      FROM    answers AS a
              INNER JOIN questions AS q ON a.question = q.question
              INNER JOIN responses AS r ON r.answer = a.answer
      WHERE   r.participant = p_participant AND r.score <= 0
      GROUP   BY r.participant
    ), '[none]');
  END IF;

  RETURN NULL;
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fn_get_num_confident` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE FUNCTION `fn_get_num_confident`(
  p_participant MEDIUMINT UNSIGNED
) RETURNS int(11)
BEGIN
  RETURN (
    SELECT  COUNT(*)
    FROM    responses
    WHERE   score > 0 AND likert = 5 AND participant = p_participant
  );
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fn_get_pathway` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE FUNCTION `fn_get_pathway`(
  p_participant MEDIUMINT(6) UNSIGNED
) RETURNS char(2) CHARSET utf8
BEGIN
  DECLARE p_pathway CHAR(2);
  DECLARE n_responses, n_correct TINYINT UNSIGNED;

  -- get FCGT responses with positive score
  SELECT  COUNT(*), SUM(CASE WHEN r.score > 0 THEN 1 ELSE 0 END)
  INTO    n_responses, n_correct
  FROM    responses AS r
          INNER JOIN questions AS q ON r.question = q.question
  WHERE   r.participant = p_participant 
          AND q.quiz = 'FCGT';

  -- only assign if FCGT is complete
  IF n_responses < 8 THEN
    RETURN NULL;
  END IF;

  -- need to assign based on FCGT responses
  RETURN CASE WHEN n_correct <= 4 
    -- <= 4 correct, cystic fibrosis
    THEN 'CF' 
    -- >= 5 correct, cardiac health
    ELSE 'CH'
  END;
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fn_get_speedy_streak` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE FUNCTION `fn_get_speedy_streak`(
  p_participant MEDIUMINT UNSIGNED
) RETURNS int(11)
BEGIN
  DECLARE n_speedy    TINYINT UNSIGNED;
  DECLARE p_timeBonus TINYINT UNSIGNED;
  -- this flag will be set to true when cursor reaches end of table
  DECLARE exit_loop BOOLEAN;   
    
    -- get responses with most recently answered first
  DECLARE c_responses CURSOR FOR
  SELECT  CASE WHEN score > 0 THEN COALESCE(timeBonus, 0) ELSE 0 END AS timeBonus
  FROM    responses
  WHERE   participant = p_participant
          AND answer IS NOT NULL 
          AND likert IS NOT NULL 
  ORDER   BY responseId DESC;
  -- return zero if no responses found
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET exit_loop = TRUE;

  SET   n_speedy = 0;
  SET   exit_loop = 0;
  OPEN  c_responses;

  -- iterate over responses
    l_responses: LOOP
    -- get time bonus
    FETCH c_responses
    INTO  p_timeBonus;
    
    CASE  p_timeBonus
      -- if time bonus is 15, increment n_timeBonus
      WHEN 15 THEN
        IF exit_loop != 1 THEN
          SET n_speedy = n_speedy + 1;
        END IF;
      -- otherwise, the streak is broken and we should break
      ELSE
        SET exit_loop = 1;
    END CASE;

    IF exit_loop THEN
        CLOSE c_responses;
        LEAVE l_responses;
    END IF;
  END LOOP l_responses;
  
  -- return the number of responses in the current speedy streak
  RETURN n_speedy;

END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fn_new_participant` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE FUNCTION `fn_new_participant`() RETURNS mediumint(6) unsigned
BEGIN
  DECLARE p_participant MEDIUMINT(6) UNSIGNED;

  -- generate random six digit number
  SET p_participant = fn_random(100000, 999999);

  -- regenerate if duplicate
  IF EXISTS (SELECT * FROM participants WHERE participant = p_participant) THEN
    RETURN fn_new_participant();
  END IF;

  -- insert the new participant
  INSERT 
  INTO    participants (participant) 
  VALUES  (p_participant);

  -- return the inserted participant
  RETURN  p_participant;
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fn_random` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE FUNCTION `fn_random`(min INTEGER, max INTEGER) RETURNS int(11)
BEGIN
  -- returns a random integer between min and max, inclusive
  RETURN FLOOR(RAND() * (max - min + 1)) + min;
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_begin_badge_quiz` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `sp_begin_badge_quiz`(
  IN  p_participant   MEDIUMINT(6) UNSIGNED,
  IN  p_badge       VARCHAR(3)
)
BEGIN
  DECLARE completeCount TINYINT UNSIGNED;

  SELECT  SUM(CASE WHEN completed = 1 THEN 1 ELSE 0 END)
  INTO    completeCount
  FROM    vw_quiz_status
  WHERE   participant = p_participant;

  IF  completeCount >= 2 AND completeCount <= 5 
    AND NOT EXISTS (
      SELECT  * 
      FROM    q_participant_quiz 
      WHERE   participant = p_participant AND quiz = p_badge
    )
  THEN
    INSERT
    INTO    q_participant_quiz (participant, quiz)
    VALUES  (p_participant, p_badge);
  END IF;
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_build_interaction_times` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `sp_build_interaction_times`()
BEGIN
  -- start record (when infoBit first went to 1)
  DECLARE s_interaction BIGINT    UNSIGNED;
  DECLARE s_clientStamp DATETIME(3);

  -- current record
  DECLARE c_participant MEDIUMINT UNSIGNED;
  DECLARE c_interaction BIGINT    UNSIGNED;
  DECLARE c_quiz        VARCHAR(6);
  DECLARE c_question    SMALLINT  UNSIGNED;
  DECLARE c_video       TINYINT   UNSIGNED;
  DECLARE c_track       VARCHAR(8);
  DECLARE c_infoBit     BIT;
  DECLARE c_clientStamp DATETIME(3);

  -- previous record
  DECLARE p_participant MEDIUMINT UNSIGNED;
  DECLARE p_interaction BIGINT    UNSIGNED;
  DECLARE p_quiz        VARCHAR(6);
  DECLARE p_question    SMALLINT  UNSIGNED;
  DECLARE p_video       TINYINT   UNSIGNED;
  DECLARE p_track       VARCHAR(8);
  DECLARE p_infoBit     BIT;
  DECLARE p_clientStamp DATETIME(3);

  -- this flag will be set to true when cursor reaches end of table
  DECLARE exit_loop     BOOLEAN;   
    
    -- get interactions
  DECLARE cur CURSOR FOR
  SELECT  participant, interaction, quiz, question, video, track, infoBit, COALESCE(clientStamp, serverStamp)
  FROM    interactions
  WHERE   track IN ('focus', 'youtube')
          AND infoBit IS NOT NULL
  ORDER   BY participant, quiz, question, video, track, COALESCE(clientStamp, serverStamp);

  -- exit loop when out of rows
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET exit_loop = TRUE;

  DECLARE exit handler for sqlexception
    BEGIN
      -- ERROR
    ROLLBACK;
    RESIGNAL;
  END;

  DECLARE exit handler for sqlwarning
   BEGIN
      -- WARNING
   ROLLBACK;
   RESIGNAL;
  END;
  
  START TRANSACTION;
  
  TRUNCATE TABLE interaction_times;

  SET   exit_loop = 0;
  OPEN  cur;

  -- iterate over responses
    looper: LOOP
    -- get time bonus
    FETCH cur
    INTO  c_participant, c_interaction, c_quiz, c_question, c_video, c_track, c_infoBit, c_clientStamp;

    -- see if we're still tracking the same thing
    IF    p_participant             = c_participant AND
          COALESCE(p_quiz, 'NULL')  = COALESCE(c_quiz, 'NULL') AND
          COALESCE(p_question, 0)   = COALESCE(c_question, 0) AND
          COALESCE(p_video, 0)      = COALESCE(c_video, 0) AND
          COALESCE(p_track, 'NULL') = COALESCE(c_track, 'NULL')
    THEN -- tracking not changed
      -- if this is an end and we have a start
      IF  s_interaction IS NOT NULL AND c_infoBit = 0 THEN
        -- insert the interaction
        INSERT
        INTO    interaction_times (
          participant, quiz, question, video, track, 
          startId, endId, startTime, endTime
        )
        VALUES  (
          p_participant, p_quiz, p_question, p_video, p_track, 
          s_interaction, c_interaction, s_clientStamp, c_clientStamp
        );
        -- clear the start sentinel
        SET s_interaction = NULL, s_clientStamp = NULL;
      END IF;
    ELSE -- tracking changed
      -- and there's a previous interaction that has at least two open stamps, insert it
      IF s_interaction IS NOT NULL AND s_interaction <> p_interaction THEN
        INSERT
        INTO    interaction_times (
          participant, quiz, question, video, track, 
          startId, endId, startTime, endTime
        )
        VALUES  (
          p_participant, p_quiz, p_question, p_video, p_track, 
          s_interaction, p_interaction, s_clientStamp, p_clientStamp
        );
        -- clear the start sentinel
        SET s_interaction = NULL, s_clientStamp = NULL;
      END IF;
    END IF;

    -- if this new interaction is up and there's not an open interaction, set start sentinel
    IF  s_interaction IS NULL AND c_infoBit = 1 THEN
      SET s_interaction = c_interaction, s_clientStamp = c_clientStamp;
    END IF;

    -- exit loop if set
    IF exit_loop THEN
        CLOSE cur;
        LEAVE looper;
    END IF;

    -- set previous variables to current data before looping
    SET p_participant = c_participant;
    SET p_interaction = c_interaction;
    SET p_infoBit     = c_infoBit;
    SET p_quiz        = c_quiz;
    SET p_question    = c_question;
    SET p_video       = c_video;
    SET p_track       = c_track;
    SET p_infoBit     = c_infoBit;
    SET p_clientStamp = c_clientStamp;
  
  END LOOP looper;

  COMMIT;
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_copy_quiz_questions` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `sp_copy_quiz_questions`(
  IN  p_from_quiz VARCHAR(6),
  IN  p_to_quiz VARCHAR(6)
)
BEGIN
  BLOCK1: begin
    declare p_from_question, p_to_question int;                     
    declare no_more_questions boolean default FALSE;  
    declare cur_questions cursor for              
      select  question 
      from    questions
      where   quiz = p_from_quiz;
    declare continue handler for not found  
      set no_more_questions := TRUE;  

    open cur_questions;
    LOOP1: loop
      fetch cur_questions
      into  p_from_question;
      if no_more_questions then
        close cur_questions;
        leave LOOP1;
      end if;
      BLOCK2: begin
        declare p_from_answer int;
        declare no_more_answers boolean default FALSE;
        declare cur_answers cursor for
          select answer
          from   answers
          where  question = p_from_question;
        declare continue handler for not found
          set no_more_answers := TRUE;
        
        INSERT 
        INTO    questions ( 
                  quiz, displayOrder, introHtml, likertIntroHtml, multiIntroHtml, 
                  header, questionFormat, likertMinLabel, likertMaxLabel 
                )
        SELECT  p_to_quiz, displayOrder, introHtml, likertIntroHtml, multiIntroHtml, 
                header, questionFormat, likertMinLabel, likertMaxLabel
        FROM    questions
        WHERE   question = p_from_question;
        
        SET     p_to_question = LAST_INSERT_ID();
    
        INSERT
        INTO    answers ( question, answerText, score, isOther, displayOrder )
        SELECT  p_to_question, answerText, score, isOther, displayOrder
        FROM    answers
        WHERE   question = p_from_question;

      end BLOCK2;
    end loop LOOP1;
  end BLOCK1;

END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_current_question` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `sp_get_current_question`(
  IN  p_participant   MEDIUMINT(6) UNSIGNED,
  IN  p_quiz          VARCHAR(6),
  OUT p_question      SMALLINT UNSIGNED,
  OUT p_questionGroup TINYINT UNSIGNED,
  OUT p_showLikert    BIT,
  OUT p_showMulti     BIT
)
BEGIN
  DECLARE p_likert              TINYINT UNSIGNED;
  DECLARE p_answer              SMALLINT UNSIGNED;
  DECLARE q_showLikert          BIT;
  DECLARE q_showMulti           BIT;
  DECLARE q_renderLikertBefore  BIT;

  IF p_participant IS NOT NULL AND p_quiz IS NOT NULL THEN

    -- lookup first unanswered question in quiz, with format
    SELECT  s.question, s.questionGroup, f.showLikert, f.showMulti, f.renderLikertBefore
    INTO    p_question, p_questionGroup, q_showLikert, q_showMulti, q_renderLikertBefore
    FROM    vw_question_status AS s
            INNER JOIN vw_question_format AS f ON s.question = f.question
    WHERE   s.participant = p_participant AND s.quiz = p_quiz AND s.completed = 0
    ORDER   BY displayOrder
    LIMIT   1;

    -- GROUP: ensure responses exist for these participant-questions
    IF  p_questionGroup IS NOT NULL
    THEN
      -- try to insert a response for each question in the group
            -- (will fail to insert if existing response)
      INSERT
      INTO    responses (participant, question, sentMulti, sentLikert)
      SELECT  p_participant, q.question, 
              CASE WHEN f.showMulti  = 1 THEN NOW(3) ELSE NULL END, 
              CASE WHEN f.showLikert = 1 THEN NOW(3) ELSE NULL END
      FROM    questions AS q
              INNER JOIN vw_question_format AS f ON q.question = f.question
              LEFT JOIN  responses AS r ON q.question = r.question AND r.participant = p_participant
      WHERE   q.quiz = p_quiz AND q.questionGroup = p_questionGroup
              AND r.participant IS NULL;
      -- pass both likert and multi info to client
      SET     p_showLikert = 1;
      SET     p_showMulti  = 1;
    -- SINGLE: ensure response exists for this participant-question
    ELSEIF  p_question IS NOT NULL
    THEN 
      -- see if response exists
      IF  EXISTS (SELECT * FROM responses WHERE participant = p_participant AND question = p_question)
      THEN
        -- get response
        SELECT  r.likert, r.answer
        INTO    p_likert, p_answer
        FROM    responses AS r
                INNER JOIN vw_question_format AS q ON r.question = q.question
        WHERE   r.participant = p_participant
                AND r.question = p_question;
        -- decide whether to display multi
        SET p_showMulti = 
          CASE WHEN
            q_showMulti = 1       -- if has multiple choice AND:
            AND (
              q_showLikert = 0      -- a) doesn't have a likert OR
              OR q_renderLikertBefore = 0 -- b) likert comes second OR
              OR p_likert IS NOT NULL   -- c) has an answered likert OR
              OR p_answer IS NOT NULL   -- d) has an answered multiple choice
            ) 
          THEN 1 
          ELSE 0 
        END;
        -- decide whether to display likert
        SET p_showLikert = 
          CASE WHEN
            q_showLikert = 1      -- if has likert AND:
            AND (
              q_showMulti = 0       -- a) doesn't have a multiple choice OR
              OR q_renderLikertBefore = 1 -- b) likert comes first OR
              OR p_answer IS NOT NULL   -- c) has an answered multiple choice OR
              OR p_likert IS NOT NULL   -- d) has an answered likert
            )
          THEN 1 
          ELSE 0 
        END;
                -- update sent timestamps
        UPDATE  responses
        SET     sentLikert = COALESCE(sentLikert, CASE WHEN p_showLikert = 1 THEN NOW(3) ELSE NULL END),
                sentMulti  = COALESCE(sentMulti,  CASE WHEN p_showMulti  = 1 THEN NOW(3) ELSE NULL END)
        WHERE   participant = p_participant AND question = p_question;
      -- insert new response if not exists
      ELSE
        -- decide whether to display multi
          SET p_showMulti = 
          CASE WHEN
            q_showMulti = 1       -- if has multiple choice AND:
            AND (
              q_showLikert = 0      -- a) doesn't have a likert OR
              OR q_renderLikertBefore = 0 -- b) likert comes second
            ) 
          THEN 1 
          ELSE 0 
        END;
        -- decide whether to display likert
        SET p_showLikert = 
          CASE WHEN
            q_showLikert = 1      -- if has likert AND:
            AND (
              q_showMulti = 0       -- a) doesn't have a multiple choice OR
              OR q_renderLikertBefore = 1 -- b) likert comes first
            )
          THEN 1 
          ELSE 0 
        END;
                -- insert empty response
        INSERT  
        INTO  responses (participant, question, sentMulti, sentLikert)
        VALUES  (
          p_participant, p_question, 
          CASE WHEN p_showMulti  = 1 THEN NOW(3) ELSE NULL END, 
          CASE WHEN p_showLikert = 1 THEN NOW(3) ELSE NULL END
        );
      END IF;
    END IF;

  END IF;

END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_current_quiz` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `sp_get_current_quiz`(
  IN  p_participant   MEDIUMINT(6) UNSIGNED,
  IN  p_pathway       CHAR(2),
  IN  p_experience    CHAR(1),
  IN  p_finishedQuiz  VARCHAR(6),
  OUT p_quiz          VARCHAR(6),
  OUT p_canGamify     BIT
)
BEGIN
  DECLARE n_gamified, n_control TINYINT UNSIGNED;
  DECLARE n_quizzes, n_completed_quizzes TINYINT UNSIGNED;
  
  -- load first unfinished quiz
  SELECT  quiz
  INTO    p_quiz
  FROM    vw_quiz_status
  WHERE   participant = p_participant AND completed = 0
  ORDER   BY q_id
  LIMIT   1;

  -- if not found, determine next quiz
  IF p_quiz IS NULL THEN

    -- get quiz counts for participant
    SELECT  COUNT(*), COALESCE(SUM(completed), 0)
    INTO    n_quizzes, n_completed_quizzes
    FROM    vw_quiz_status
    WHERE   participant = p_participant;

    -- if not found, decide what to do next by seeing where the participant is in the flow
    CASE
      -- if they haven't started any quizzes
      WHEN n_quizzes = 0 THEN
        -- everyone starts with the FCGT
        SET   p_quiz = fn_enqueue_quiz(p_participant, 'FCGT');
      -- if they completed the FCGT and nothing else
      WHEN n_quizzes = 1 THEN
        SELECT  pathway, experience
        INTO    p_pathway, p_experience
        FROM    participants
        WHERE   participant = p_participant;
                
        IF  p_pathway IS NOT NULL AND p_experience IS NOT NULL THEN
          -- next quiz is first PGEN
          SET   p_quiz =  fn_enqueue_quiz(p_participant, 'PGEN1');
        ELSE
          -- completed FCGT, need to assign pathway and randomize UX
          SET   p_pathway = fn_get_pathway(p_participant);
          SET   p_experience = fn_get_experience(p_pathway);

          -- update participant with assignments
          UPDATE  participants
          SET     pathway = p_pathway, experience = p_experience
          WHERE   participant = p_participant;
                    
          -- don't immediately enqueue PGEN1 if gamified (so interstitial shows)
          IF  p_experience != 'G' OR p_finishedQuiz IS NULL THEN
            SET   p_quiz =  fn_enqueue_quiz(p_participant, 'PGEN1');
          END IF;
        END IF;

      -- if badge quizzes are completed
      WHEN n_quizzes = 5 AND n_completed_quizzes = 5 THEN
        -- choose vignette
        IF  p_experience != 'G' OR p_finishedQuiz IS NULL THEN
          SET   p_quiz = fn_enqueue_quiz(p_participant, fn_assign_vignette(p_participant, p_pathway));
        END IF;

      -- if vignette is completed
      WHEN n_quizzes = 6 AND n_completed_quizzes = 6 THEN
        -- don't immediately enqueue PGEN2 if participant just finished vignette
        IF  p_experience != 'G' OR p_finishedQuiz IS NULL THEN
          SET   p_quiz =  fn_enqueue_quiz(p_participant, 'PGEN2');
        END IF;

      -- if PGEN2 is completed
      WHEN  n_quizzes = 7 AND n_completed_quizzes = 7 THEN
        -- demographics
        IF  p_experience != 'G' OR p_finishedQuiz IS NULL THEN
          SET   p_quiz = fn_enqueue_quiz(p_participant, 'DEMO');
        END IF;

      -- if demographics are completed
      WHEN  n_quizzes = 8 AND n_completed_quizzes = 8 THEN
        -- feedback
        SET   p_quiz = fn_enqueue_quiz(p_participant, 'FEED');

      -- otherwise, do nothing
      ELSE BEGIN END;
    END CASE;
  END IF;

  -- see if this participant and this quiz can be gamified
  SELECT  CASE WHEN (SELECT experience FROM participants WHERE participant = p_participant) = 'G' AND canGamify = 1 
          THEN 1 ELSE 0 END
  INTO    p_canGamify
  FROM    vw_quiz_format
  WHERE   quiz = p_quiz;
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_interaction` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `sp_interaction`(
  IN  p_participant MEDIUMINT UNSIGNED,
  IN  p_quiz        VARCHAR(6),
  IN  p_question    SMALLINT UNSIGNED,
  IN  p_answer      SMALLINT UNSIGNED,
  IN  p_video       TINYINT UNSIGNED,
  IN  p_track       VARCHAR(8),
  IN  p_infoText    VARCHAR(160),
  IN  p_infoInt     INT,
  IN  p_infoByte    TINYINT,
  IN  p_infoBit     BIT,
  IN  p_clientStamp DATETIME(3)
)
BEGIN
  IF (p_participant IS NOT NULL) THEN
    INSERT
    INTO    interactions (participant, quiz, question, answer, video, track, infoText, infoInt, infoByte, infoBit, clientStamp, serverStamp)
    VALUES  (p_participant, p_quiz, p_question, p_answer, p_video, p_track, p_infoText, p_infoInt, p_infoByte, p_infoBit, p_clientStamp, NOW(3));
    
    SELECT  LAST_INSERT_ID() AS interaction;
  END IF;
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_load_participant` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `sp_load_participant`(
  INOUT p_participant       MEDIUMINT(6) UNSIGNED,
  INOUT p_consented         DATETIME(3),
  INOUT p_isTest            BIT,
  INOUT p_isExpired         BIT,
  OUT   p_pathway           CHAR(2),
  OUT   p_experience        CHAR(1),
  OUT   p_finishedStamp     DATETIME(3)
)
BEGIN
  -- generate participant id if needed
  IF p_participant IS NULL THEN
    SET p_participant = fn_new_participant();
  -- insert participant ID if not exists
  ELSEIF NOT EXISTS (SELECT * FROM participants WHERE participant = p_participant) THEN
    INSERT
    INTO    participants(participant)
    VALUES  (p_participant);
  END IF;

  -- record consent
  IF p_consented IS NOT NULL THEN
    UPDATE  participants
    SET     consented = COALESCE(consented, p_consented)
    WHERE   participant = p_participant;
  END IF;

  -- record test user
  IF p_isTest = 1 THEN
    UPDATE  participants
    SET     isTest = 1
    WHERE   participant = p_participant;
  END IF;

  -- select participant details
  SELECT  participant, consented, pathway, experience, finishedStamp, isTest,
          CASE WHEN DATE_ADD(consented, INTERVAL 7 DAY) < NOW(3)  THEN 1 ELSE 0 END
  INTO    p_participant, p_consented, p_pathway, p_experience, p_finishedStamp, p_isTest, p_isExpired
  FROM    participants
  WHERE   participant = p_participant;

END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_me` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `sp_me`(
  IN  p_participant       MEDIUMINT(6) UNSIGNED,
  IN  p_consented         DATETIME(3),
  IN  p_question          SMALLINT UNSIGNED,
  IN  p_answer            SMALLINT UNSIGNED,
  IN  p_answers           VARCHAR(255),
  IN  p_likert            TINYINT(1) UNSIGNED,
  IN  p_displayedLikert   DATETIME(3),
  IN  p_answeredLikert    DATETIME(3),
  IN  p_displayedMulti    DATETIME(3),
  IN  p_answeredMulti     DATETIME(3),
  IN  p_other             VARCHAR(60),
  IN  p_badge             VARCHAR(3),
  IN  p_startBadgeQuiz    BIT,
  IN  p_isTest            BIT
)
BEGIN
  -- declare additional variables 
  DECLARE p_pathway       CHAR(2);
  DECLARE p_experience    CHAR(1);
  DECLARE p_quiz          VARCHAR(6);
  DECLARE p_qAnswered     SMALLINT UNSIGNED;
  DECLARE p_prevScore     TINYINT;
  DECLARE p_prevTimeBonus TINYINT UNSIGNED;
  DECLARE p_isExpired     BIT;
  DECLARE p_showLikert    BIT;
  DECLARE p_showMulti     BIT;
  DECLARE p_canGamify     BIT;
  DECLARE p_couldGamify   BIT;
  DECLARE p_numConfident  TINYINT UNSIGNED;
  DECLARE p_speedyStreak  TINYINT UNSIGNED;
  DECLARE p_questionGroup TINYINT UNSIGNED;
  DECLARE p_finishedQuiz  VARCHAR(6);
  DECLARE p_finishedStamp DATETIME(3);
    
  -- load participant data
  CALL sp_load_participant(p_participant, p_consented, p_isTest, p_isExpired, p_pathway, p_experience, p_finishedStamp);

  -- ensure that the participant has consented and hasn't expired
  IF p_consented IS NOT NULL AND p_isExpired = 0 THEN 

    -- record response if present
    IF p_question IS NOT NULL THEN
      CALL sp_response(
                p_participant, p_question, p_answer, p_answers, p_likert, 
                p_displayedLikert, p_answeredLikert, p_displayedMulti, p_answeredMulti, 
                p_other, p_finishedStamp, p_prevScore, p_prevTimeBonus, p_finishedQuiz, p_couldGamify);
    END IF;

    -- don't advance through the instrument if there's a score to report
    -- or the user is gamified and just finished PGEN1
    IF p_prevScore IS NULL THEN -- AND NOT (p_experience = 'G' AND p_finishedQuiz = 'PGEN1') THEN

      -- begin badge quiz if requested
      IF  p_startBadgeQuiz = 1 AND p_badge IS NOT NULL THEN
        CALL sp_begin_badge_quiz(p_participant, p_badge);
      END IF;

      -- compute current quiz for user
      CALL sp_get_current_quiz(p_participant, p_pathway, p_experience, p_finishedQuiz, p_quiz, p_canGamify);

      -- load current question(s)
      CALL sp_get_current_question(p_participant, p_quiz, p_question, p_questionGroup, p_showLikert, p_showMulti);
        
      -- set badge parameter if in a badge quiz
      SET p_badge = fn_get_badge(p_badge, p_quiz, p_participant);
           
    -- if scored, NULL out everything else for safety
    ELSE
      SET p_qAnswered = p_question, p_quiz = NULL, p_question = NULL, p_answer = NULL, p_likert = NULL,
        p_displayedLikert = NULL, p_answeredLikert = NULL, p_displayedMulti = NULL,
        p_answeredMulti = NULL, p_other = NULL, p_badge = NULL, p_questionGroup = NULL;
    END IF;
        
  -- if not consented, NULL out everything else for safety
  ELSE
    SET p_quiz = NULL, p_question = NULL, p_answer = NULL, p_likert = NULL,
      p_displayedLikert = NULL, p_answeredLikert = NULL, p_displayedMulti = NULL,
      p_answeredMulti = NULL, p_other = NULL, p_badge = NULL, p_questionGroup = NULL;
  END IF;

  -- select values to client

  --  0: participant and parameters
  CALL sp_select_user(p_participant, p_quiz, p_question, p_badge, p_canGamify, p_prevScore, p_finishedQuiz, p_isExpired);

  -- ensure that the participant has consented and hasn't expired
  IF p_consented IS NOT NULL AND p_isExpired = 0 THEN 

    --  1: current quiz
    CALL sp_select_quiz(p_quiz);

    --  2: questions for quiz
    CALL sp_select_quiz_questions(p_quiz);

    --  3: current question(s)
    CALL sp_select_questions(p_experience, p_quiz, p_question, p_showLikert, p_showMulti, p_questionGroup);

    --  4: answers for current question
    CALL sp_select_answers(p_question, p_quiz, p_questionGroup, p_showMulti);

    --  5: response(s) to current question(s)
    CALL sp_select_responses(p_participant, p_question, p_quiz, p_questionGroup);

    --  6: badges for pathway
    CALL sp_select_badges(p_participant, p_experience, p_badge, p_prevScore, p_finishedQuiz, p_pathway);

    --  7: videos for current badge
    CALL sp_select_videos(p_badge);

    --  8: score related information
    CALL sp_select_scores(p_participant, p_qAnswered, p_prevScore, p_prevTimeBonus);
      
    --  9: quiz scores if just finished a quiz
    CALL sp_select_quiz_scores(p_participant, p_experience, p_finishedQuiz, p_couldGamify);
      
    -- 10: scores for quiz if just finished
    CALL sp_select_last_quiz(p_participant, p_experience, p_finishedQuiz, p_couldGamify);
      
    -- 11: leaderboard scores if completed
    CALL sp_select_leaderboard(p_finishedStamp, p_experience, p_pathway);

  END IF;
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_response` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `sp_response`(
  IN  p_participant         MEDIUMINT(6) UNSIGNED,
  IN  p_question            SMALLINT UNSIGNED,
  IN  p_answer              SMALLINT UNSIGNED,
  IN  p_answers             VARCHAR(255),
  IN  p_likert              TINYINT UNSIGNED,
  IN  p_displayedLikert     DATETIME(3),
  IN  p_answeredLikert      DATETIME(3),
  IN  p_displayedMulti      DATETIME(3),
  IN  p_answeredMulti       DATETIME(3),
  IN  p_otherText           VARCHAR(60),
INOUT p_finishedStamp       DATETIME(3),
  OUT p_participantScore    TINYINT,
  OUT p_timeBonus           TINYINT UNSIGNED,
  OUT p_finishedQuiz        VARCHAR(6),
  OUT p_couldGamify         BIT
)
BEGIN
  DECLARE p_answerScore     TINYINT;
  DECLARE p_submittedLikert DATETIME(3);
  DECLARE p_sentMulti       DATETIME(3);
  DECLARE p_submittedMulti  DATETIME(3);
  DECLARE p_canGamify       BIT;
  DECLARE p_prevQuiz        VARCHAR(6);
  DECLARE p_seconds         INT;
    
  -- get response details
  SELECT  COALESCE(answer, p_answer), COALESCE(likert, p_likert), sentMulti, -- don't allow answers to be changed
          COALESCE(displayedMulti, p_displayedMulti), COALESCE(answeredMulti, p_answeredMulti), -- preserve existing times
          CASE WHEN p_answer IS NOT NULL THEN COALESCE(submittedMulti, NOW(3)) ELSE submittedMulti END, -- record time multi submitted
          CASE WHEN p_likert IS NOT NULL THEN COALESCE(submittedLikert, NOW(3)) ELSE submittedLikert END -- record time likert submitted
  INTO    p_answer, p_likert, p_sentMulti, p_displayedMulti, p_answeredMulti, p_submittedMulti, p_submittedLikert
  FROM    responses
  WHERE   participant = p_participant AND question = p_question;

  -- get answer score
  SELECT  score
  INTO    p_answerScore
  FROM    answers
  WHERE   answer = p_answer AND question = p_question;

  -- if participant answered 'Decline to answer' in multiple choice, skip likert
  IF (p_likert IS NULL AND p_answerScore = 0) THEN
    SET p_likert = 0;
  -- if participant answered 'Decline to answer' in likert, skip multiple choice
  ELSEIF (p_answer IS NULL AND p_likert = 0) THEN
    SELECT  answer
    INTO    p_answer
    FROM    answers
    WHERE   question = p_question AND score = 0;

    SET     p_answerScore = 0;
  END IF;
    
  -- calculate score
  IF (p_answer IS NOT NULL AND p_likert IS NOT NULL) THEN
    SET p_participantScore = p_answerScore * CAST(p_likert AS signed);
  END IF;

  -- calculate time bonus, don't trust client side timing
  IF (p_participantScore > 0 AND p_sentMulti IS NOT NULL AND p_submittedMulti IS NOT NULL) THEN
    SET p_seconds = TIMESTAMPDIFF(SECOND,p_sentMulti,p_submittedMulti);
    
    IF p_seconds <= 30 THEN
      SET p_timeBonus = 15;
    ELSEIF p_seconds <= 60 THEN
      SET p_timeBonus = 10;
    ELSEIF p_seconds <= 120 THEN
      SET p_timeBonus = 5;
    ELSE
      SET p_timeBonus = 0;
    END IF;
  END IF;

  -- get current quiz before recording response
  SET p_prevQuiz = fn_first_incomplete_quiz(p_participant);
    
  -- upsert the response
  IF EXISTS (SELECT * FROM responses WHERE participant = p_participant AND question = p_question) THEN
    UPDATE  responses
    SET     answer          = COALESCE(answer, p_answer),
            likert          = COALESCE(likert, p_likert),
            displayedLikert = COALESCE(displayedLikert, p_displayedLikert),
            answeredLikert  = COALESCE(answeredLikert, p_answeredLikert),
            submittedLikert = COALESCE(submittedLikert, p_submittedLikert),
            displayedMulti  = COALESCE(displayedMulti, p_displayedMulti),
            answeredMulti   = COALESCE(answeredMulti, p_answeredMulti),
            submittedMulti  = COALESCE(submittedMulti, p_submittedMulti),
            otherText       = COALESCE(otherText, p_otherText),
            score           = COALESCE(score, p_participantScore),
            timeBonus       = COALESCE(timeBonus, p_timeBonus)
    WHERE   participant     = p_participant AND
            question        = p_question;
  ELSE
    INSERT
    INTO    responses 
            ( participant, question, answer, likert, 
              displayedLikert, answeredLikert, submittedLikert, displayedMulti, 
              answeredMulti, submittedMulti, otherText, score, timeBonus )
    VALUES  ( p_participant, p_question, p_answer, p_likert,
              p_displayedLikert, p_answeredLikert, p_submittedLikert, p_displayedMulti, 
              p_answeredMulti, p_submittedMulti, p_otherText, p_participantScore, p_timeBonus );
  END IF;

  -- handle multi-multi answers
  IF p_answers IS NOT NULL THEN
    INSERT  
    INTO    responses_multi (participant, question, answer)
    SELECT  p_participant, question, answer
    FROM    answers
    WHERE   question = p_question AND FIND_IN_SET(answer, p_answers);
  END IF;

  -- see if this completed the quiz
  SELECT  CASE WHEN p_prevQuiz != fn_first_incomplete_quiz(p_participant)
            THEN p_prevQuiz
            ELSE NULL
          END
  INTO    p_finishedQuiz;
  
  -- see if last quiz was gamified
  IF p_finishedQuiz IS NOT NULL THEN
    SELECT  canGamify
    INTO    p_couldGamify
    FROM    vw_quiz_format
    WHERE   quiz = p_finishedQuiz;
  END IF;
  
  -- if completed demographics, participant is done
  IF p_finishedQuiz = 'DEMO' THEN
    -- set finished stampe and update participants table
    SET     p_finishedStamp = NOW(3);
    UPDATE  participants
    SET     finishedStamp = p_finishedStamp
    WHERE   participant = p_participant;
  END IF;
  

  -- see if participant is gamified
  SELECT  CASE WHEN experience = 'G' THEN 1 ELSE 0 END
  INTO    p_canGamify
  FROM    participants 
  WHERE   participant = p_participant;
  
  -- see if quiz answered was gamified
  IF p_canGamify = 1 THEN
    SELECT  f.canGamify
    INTO    p_canGamify
    FROM    questions AS q
            INNER JOIN vw_quiz_format AS f ON q.quiz = f.quiz
    WHERE   q.question = p_question;
  END IF;

-- clear scores if not gamified
  IF p_canGamify = 0 THEN
    SET p_participantScore = NULL;
    SET p_timeBonus = NULL;
  END IF;
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_select_answers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `sp_select_answers`(
  IN  p_question          SMALLINT UNSIGNED,
  IN  p_quiz              VARCHAR(6),
  IN  p_questionGroup     TINYINT UNSIGNED,
  IN  p_showMulti         BIT
)
BEGIN
  SELECT  a.question, a.answer, a.answerText, a.displayOrder,
          CASE WHEN a.isOther = 1 THEN 1 ELSE 0 END AS isOther
  FROM    answers AS a
          INNER JOIN questions AS q ON a.question = q.question
  WHERE   p_showMulti AND (
            a.question = p_question
            OR (p_questionGroup IS NOT NULL AND q.quiz = p_quiz AND q.questionGroup = p_questionGroup)
          )
  ORDER   BY q.displayOrder, a.displayOrder;
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_select_badges` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `sp_select_badges`(
  IN  p_participant       MEDIUMINT(6) UNSIGNED,
  IN  p_experience        CHAR(1),
  IN  p_badge             VARCHAR(3),
  IN  p_prevScore         TINYINT,
  IN  p_finishedQuiz      VARCHAR(6),
  IN  p_pathway           CHAR(2)
)
BEGIN
  SELECT  b.badge, b.badgeName, COALESCE(s.completed, 0) AS completed,
          CASE 
            -- incomplete get greyed out
            WHEN COALESCE(s.completed, 0) = 0 THEN '-mask' 
            -- gamified users get trophy for all correct ansers...
            WHEN p_experience = 'G' AND s.nCorrect = s.nMulti THEN '-trophy'
            -- or a star for at least one correct answer...
            WHEN p_experience = 'G' AND s.nCorrect > 0 THEN '-star'
            -- otherwise, just a check mark
            ELSE '-check'
          END AS iconSuffix
  FROM    badges AS b
          LEFT JOIN vw_quiz_score AS s ON b.badge = s.quiz AND s.participant = p_participant
  WHERE   (p_finishedQuiz IS NULL OR p_experience != 'G' OR p_prevScore IS NULL) AND
          ((b.badge = p_badge) OR (p_badge IS NULL AND pathway = p_pathway))
  ORDER   BY displayOrder;
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_select_last_quiz` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `sp_select_last_quiz`(
  IN  p_participant       MEDIUMINT(6) UNSIGNED,
  IN  p_experience        CHAR(1),
  IN  p_finishedQuiz      VARCHAR(6),
  IN  p_couldGamify       BIT
)
BEGIN
  
  IF  p_finishedQuiz IS NOT NULL 
      AND p_experience = 'G' 
      AND COALESCE(p_couldGamify, 0) = 1
  THEN
    SET     @rank = 0;
    SELECT  @rank:=@rank+1 AS num, lq.*
    FROM    (
              SELECT    q.question, q.displayOrder, q.multiIntroHtml, 
                      r.likert, r.score AS multiScore, COALESCE(r.timeBonus,0) AS timeBonus,
                      a.answerText AS answer, a.score AS answerScore, c.answerText AS correct,
                      COALESCE(r.score,0) + COALESCE(r.timeBonus,0) AS totalScore,
                      TIMESTAMPDIFF(SECOND,r.sentMulti,r.submittedMulti) AS seconds
              FROM    questions AS q
                      INNER JOIN responses AS r ON q.question = r.question
                      LEFT JOIN answers AS a ON r.answer = a.answer
                      LEFT JOIN answers AS c ON c.question = q.question AND c.score > 0
              WHERE   q.quiz = p_finishedQuiz AND r.participant = p_participant AND r.score IS NOT NULL
              ORDER   BY r.responseid
            ) AS lq;
  ELSE
    SELECT  1 AS skip;
  END IF;
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_select_leaderboard` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `sp_select_leaderboard`(
  IN  p_finishedStamp     DATETIME(3),
  IN  p_experience        CHAR(1),
  IN  p_pathway           CHAR(2)
)
BEGIN
  IF  p_experience = 'G' AND p_finishedStamp IS NOT NULL THEN
    SET     @rank = 0;
    SELECT  @rank:=@rank+1 AS rank, ranked.*
    FROM    (
                SELECT    p.participant, 
                        SUM(COALESCE(r.score, 0) + COALESCE(r.timeBonus, 0)) AS score
                FROM    participants AS p
                        INNER JOIN responses AS r ON p.participant = r.participant
                WHERE   p.finishedStamp IS NOT NULL AND
                        p.experience = p_experience AND
                        p.pathway = p_pathway AND
                        p.isTest = 0
                GROUP   BY p.participant
                ORDER   BY score DESC
            ) AS ranked
    ORDER   BY score DESC;  
  ELSE
    SELECT  1 AS skip;
  END IF;
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_select_questions` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `sp_select_questions`(
  IN  p_experience        CHAR(1),
  IN  p_quiz              VARCHAR(6),
  IN  p_question          SMALLINT UNSIGNED,
  IN  p_showLikert        BIT,
  IN  p_showMulti         BIT,
  IN  p_questionGroup     TINYINT UNSIGNED
)
BEGIN
  SELECT  q.question, q.introHtml, q.outroHtml, 
          CASE WHEN p_showLikert = 1 THEN q.likertIntroHtml ELSE NULL END AS likertIntroHtml,
          CASE WHEN p_showMulti  = 1 THEN q.multiIntroHtml ELSE NULL END AS multiIntroHtml,
          q.header, q.displayOrder,
          CASE WHEN p_showLikert = 1 THEN q.likertMinLabel ELSE NULL END AS likertMinLabel,
          CASE WHEN p_showLikert = 1 THEN q.likertMaxLabel ELSE NULL END AS likertMaxLabel,
          q.questionFormat, f.showLikert, f.showMulti, 
          f.renderLikertBefore, f.allowMultipleMulti,
          CASE WHEN p_experience = 'G' 
            THEN (SELECT MAX(score) FROM answers WHERE question = p_question) 
            ELSE NULL 
          END AS maxScore
  FROM    questions AS q
          INNER JOIN vw_question_format AS f ON q.question = f.question
  WHERE   q.question = p_question
          OR (p_questionGroup IS NOT NULL AND q.quiz = p_quiz AND q.questionGroup = p_questionGroup)
  ORDER   BY q.displayOrder;
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_select_quiz` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `sp_select_quiz`(
  IN  p_quiz              VARCHAR(6)
)
BEGIN
  SELECT  q.quiz, q.title, q.introHtml, f.canGamify, f.showHeader
  FROM    quizzes AS q
          INNER JOIN vw_quiz_format AS f ON q.quiz = f.quiz
  WHERE   q.quiz = p_quiz;
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_select_quiz_questions` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `sp_select_quiz_questions`(
  IN  p_quiz              VARCHAR(6)
)
BEGIN
  SELECT  question
  FROM    questions
  WHERE   quiz = p_quiz
  ORDER   BY displayOrder;
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_select_quiz_scores` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `sp_select_quiz_scores`(
  IN  p_participant       MEDIUMINT(6) UNSIGNED,
  IN  p_experience        CHAR(1),
  IN  p_finishedQuiz      VARCHAR(6),
  IN  p_couldGamify       BIT
)
BEGIN
  IF  p_finishedQuiz IS NOT NULL 
      AND p_experience = 'G' 
      AND (COALESCE(p_couldGamify, 0) = 1 OR p_finishedQuiz = 'PGEN1')
  THEN
    SET     @rank = 0;
    SELECT  @rank:=@rank+1 AS num, qs.*
    FROM    (
              SELECT  s.quiz, q.title, 
                      s.multiScore, s.timeBonuses, s.totalScore, s.isScored
              FROM    vw_quiz_score AS s
                      INNER JOIN q_participant_quiz AS qpq ON s.quiz = qpq.quiz AND s.participant = qpq.participant
                      INNER JOIN quizzes AS q ON s.quiz = q.quiz
              WHERE   s.participant = p_participant
              ORDER   BY qpq.q_id
            ) AS qs;
  ELSE
    SELECT  1 AS skip;
  END IF;

END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_select_responses` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `sp_select_responses`(
  IN  p_participant       MEDIUMINT(6) UNSIGNED,
  IN  p_question          SMALLINT UNSIGNED,
  IN  p_quiz              VARCHAR(6),
  IN  p_questionGroup     TINYINT UNSIGNED
)
BEGIN
  SELECT  r.participant, r.question, r.answer, r.likert, r.sentMulti,  
          -- if displayedMulti is not defined
          COALESCE(
            r.displayedMulti, 
            -- and it's been more than 2 minutes since we first sent the multi
            CASE WHEN r.sentMulti IS NOT NULL AND DATE_ADD(r.sentMulti, INTERVAL 2 MINUTE) < NOW(3) 
              -- send tomorrow, to avoid time zone mess
              THEN DATE_ADD(r.sentMulti, INTERVAL 1 DAY) 
              ELSE NULL 
            END
          ) AS displayedMulti,
          r.answeredMulti, r.submittedMulti, r.sentLikert, r.displayedLikert, r.answeredLikert, r.submittedLikert
  FROM    responses AS r
          INNER JOIN questions AS q ON r.question = q.question
  WHERE   r.participant = p_participant AND 
          ( 
            r.question = p_question OR 
            (p_questionGroup IS NOT NULL AND q.quiz = p_quiz AND q.questionGroup = p_questionGroup)
          )
  ORDER   BY q.displayOrder;
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_select_scores` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `sp_select_scores`(
  IN  p_participant       MEDIUMINT(6) UNSIGNED,
  IN  p_question          SMALLINT UNSIGNED,
  IN  p_prevScore         TINYINT,
  IN  p_prevTimeBonus     TINYINT UNSIGNED
)
BEGIN
  DECLARE p_quiz          VARCHAR(6);
  DECLARE p_questionGroup TINYINT UNSIGNED;
  
  IF  p_prevScore IS NOT NULL THEN
    SELECT  quiz, questionGroup
    INTO    p_quiz, p_questionGroup
    FROM    questions
    WHERE   question = p_question;
  
    SELECT  q.question, r.score, r.timeBonus,
            r.likert, q.multiIntroHtml AS question, 
            a.answerText AS answered, c.answerText AS correct
    FROM    questions AS q
            LEFT JOIN responses AS r ON r.question = q.question
            LEFT JOIN answers AS a ON r.answer = a.answer
            LEFT JOIN answers AS c ON c.question = q.question AND c.score > 0
    WHERE   r.participant = p_participant AND 
            (
              q.question = p_question OR
              (q.quiz = p_quiz AND q.questionGroup = p_questionGroup)
            )
    ORDER   BY q.displayOrder;
  ELSE
    SELECT  1 AS skip;
  END IF;

END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_select_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `sp_select_user`(
  IN  p_participant       MEDIUMINT(6) UNSIGNED,
  IN  p_quiz              VARCHAR(6),
  IN  p_question          SMALLINT UNSIGNED,
  IN  p_badge             VARCHAR(3),
  IN  p_canGamify         BIT,
  IN  p_prevScore         TINYINT,
  IN  p_finishedQuiz      VARCHAR(6),
  IN  p_isExpired         BIT
)
BEGIN
  SELECT  participant, consented, experience, experience AS exp, pathway,
          p_badge AS badge, p_quiz AS quiz, p_question AS question,
          CASE WHEN experience = 'G' AND (p_prevScore IS NOT NULL OR p_canGamify = 1 OR p_finishedQuiz IS NOT NULL)
            THEN (SELECT SUM(COALESCE(score,0)) + SUM(COALESCE(timeBonus,0)) FROM responses WHERE participant = p_participant)
            ELSE NULL 
          END AS score, 
          CASE WHEN experience = 'G' AND (p_canGamify = 1 OR p_prevScore IS NOT NULL)
            THEN fn_get_speedy_streak(p_participant)
            ELSE NULL 
          END AS speedyStreak, 
          CASE WHEN experience = 'G' AND (p_canGamify = 1 OR p_prevScore IS NOT NULL)
            THEN fn_get_num_confident(p_participant) 
            ELSE NULL 
          END AS numConfident,
          fn_get_missed_words(p_participant) AS missedWords,
          p_finishedQuiz AS finishedQuiz, finishedStamp,
          CASE WHEN consented IS NOT NULL AND finishedStamp IS NULL 
            THEN DATE_ADD(consented, INTERVAL 7 DAY) 
            ELSE NULL 
          END AS deadline,
          CASE WHEN experience = 'G' AND finishedStamp IS NOT NULL
            THEN (SELECT COUNT(*) FROM participants WHERE finishedStamp IS NOT NULL AND isTest = 0)
            ELSE NULL
          END AS participantCount,
          CASE WHEN isTest = 1 THEN 1 ELSE 0 END AS isTest,
          CASE WHEN p_isExpired = 1 THEN 1 ELSE 0 END AS isExpired
  FROM    participants
  WHERE   participant = p_participant;
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_select_videos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `sp_select_videos`(
  IN  p_badge             VARCHAR(3)
)
BEGIN
  SELECT  video, youtube, badge, title, user
  FROM    videos
  WHERE   badge = p_badge
  ORDER   BY displayOrder;
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_view` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
DELIMITER ;;
CREATE PROCEDURE `sp_view`(
  IN  p_participant   MEDIUMINT(6) UNSIGNED,
  IN  p_youtube       VARCHAR(12) ,
  IN  p_recordedAt    DATETIME(3),
  IN  p_quiz          VARCHAR(6),
  IN  p_question      SMALLINT UNSIGNED,
  IN  p_videoTime     DECIMAL(12, 6),
  IN  p_playerState   TINYINT
)
BEGIN
  DECLARE p_video TINYINT UNSIGNED;
    
    -- lookup video id
    SELECT  video
    INTO    p_video
    FROM    videos
    WHERE   youtube = p_youtube;
    
    -- insert view
    INSERT
    INTO    views (participant, recordedAt, video, quiz, question, videoTime, playerState)
    VALUES  (p_participant, p_recordedAt, p_video, p_quiz, p_question, p_videoTime, p_playerState);
    
    SELECT  LAST_INSERT_ID() AS inserted_view;
END ;;
DELIMITER ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `vw_interaction_time_summary`
--

/*!50001 DROP VIEW IF EXISTS `vw_interaction_time_summary`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 */
/*!50001 VIEW `vw_interaction_time_summary` AS select `interaction_times`.`participant` AS `participant`,sum((case when (`interaction_times`.`track` = 'focus') then (`interaction_times`.`endTime` - `interaction_times`.`startTime`) else 0 end)) AS `focusTime`,sum((case when (`interaction_times`.`track` = 'youTube') then (`interaction_times`.`endTime` - `interaction_times`.`startTime`) else 0 end)) AS `videoTime` from `interaction_times` where ((`interaction_times`.`endTime` is not null) and (`interaction_times`.`startTime` is not null) and (`interaction_times`.`track` in ('focus','youtube'))) group by `interaction_times`.`participant` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_participant_score`
--

/*!50001 DROP VIEW IF EXISTS `vw_participant_score`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 */
/*!50001 VIEW `vw_participant_score` AS select `responses`.`participant` AS `participant`,(sum(coalesce(`responses`.`score`,0)) + sum(coalesce(`responses`.`timeBonus`,0))) AS `score` from `responses` group by `responses`.`participant` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_question_format`
--

/*!50001 DROP VIEW IF EXISTS `vw_question_format`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 */
/*!50001 VIEW `vw_question_format` AS select `questions`.`question` AS `question`,((`questions`.`questionFormat` & 1) > 0) AS `showLikert`,((`questions`.`questionFormat` & 2) > 0) AS `showMulti`,((`questions`.`questionFormat` & 4) > 0) AS `renderLikertBefore`,((`questions`.`questionFormat` & 16) > 0) AS `allowMultipleMulti`,((`questions`.`questionFormat` & 32) > 0) AS `isPunnett` from `questions` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_question_status`
--

/*!50001 DROP VIEW IF EXISTS `vw_question_status`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 */
/*!50001 VIEW `vw_question_status` AS select `p`.`participant` AS `participant`,`q`.`quiz` AS `quiz`,`q`.`displayOrder` AS `displayOrder`,`q`.`question` AS `question`,`q`.`questionGroup` AS `questionGroup`,(case when (((((`q`.`questionFormat` & 1) = 0) or (`r`.`likert` is not null)) and (((`q`.`questionFormat` & 2) = 0) or (`r`.`answer` is not null))) or ((select count(0) from `responses_multi` `rm` where ((`q`.`question` = `rm`.`question`) and (`p`.`participant` = `rm`.`participant`))) > 0)) then 1 else 0 end) AS `completed` from ((`questions` `q` join `participants` `p`) left join `responses` `r` on(((`p`.`participant` = `r`.`participant`) and (`q`.`question` = `r`.`question`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_quiz_format`
--

/*!50001 DROP VIEW IF EXISTS `vw_quiz_format`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 */
/*!50001 VIEW `vw_quiz_format` AS select `quizzes`.`quiz` AS `quiz`,`quizzes`.`quizFormat` AS `quizFormat`,((`quizzes`.`quizFormat` & 1) > 0) AS `canGamify`,((`quizzes`.`quizFormat` & 2) > 0) AS `showHeader` from `quizzes` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_quiz_score`
--

/*!50001 DROP VIEW IF EXISTS `vw_quiz_score`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 */
/*!50001 VIEW `vw_quiz_score` AS select `r`.`participant` AS `participant`,`q`.`quiz` AS `quiz`,sum((case when ((`q`.`questionFormat` & 1) > 0) then 1 else 0 end)) AS `nLikert`,count(`r`.`likert`) AS `cLikert`,sum((case when ((`q`.`questionFormat` & 2) > 0) then 1 else 0 end)) AS `nMulti`,count(`r`.`answer`) AS `cMulti`,sum((case when (`r`.`score` > 0) then 1 else 0 end)) AS `nCorrect`,min((case when ((((`q`.`questionFormat` & 1) = 0) or (`r`.`likert` is not null)) and (((`q`.`questionFormat` & 2) = 0) or (`r`.`answer` is not null))) then 1 else 0 end)) AS `completed`,sum(coalesce(`r`.`score`,0)) AS `multiScore`,sum(coalesce(`r`.`timeBonus`,0)) AS `timeBonuses`,sum((coalesce(`r`.`score`,0) + coalesce(`r`.`timeBonus`,0))) AS `totalScore`,coalesce(`s`.`isScored`,0) AS `isScored` from ((`helicase_dev`.`responses` `r` join `helicase_dev`.`questions` `q` on((`q`.`question` = `r`.`question`))) left join (select `q2`.`quiz` AS `quiz`,(case when (max(`a2`.`score`) > 0) then 1 else 0 end) AS `isScored` from (`helicase_dev`.`answers` `a2` join `helicase_dev`.`questions` `q2` on((`a2`.`question` = `q2`.`question`))) group by `q2`.`quiz`) `s` on((`q`.`quiz` = `s`.`quiz`))) group by `r`.`participant`,`q`.`quiz` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_quiz_status`
--

/*!50001 DROP VIEW IF EXISTS `vw_quiz_status`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 */
/*!50001 VIEW `vw_quiz_status` AS select `q`.`q_id` AS `q_id`,`q`.`participant` AS `participant`,`q`.`quiz` AS `quiz`,(select min(`vw_question_status`.`completed`) from `vw_question_status` where ((`vw_question_status`.`participant` = `q`.`participant`) and (`vw_question_status`.`quiz` = `q`.`quiz`))) AS `completed` from `q_participant_quiz` `q` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed
