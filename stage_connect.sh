#!/bin/bash
mysql \
	-h $STAGE_HOST \
	-P $STAGE_PORT \
	-u $STAGE_MASTER_USER \
	-p$STAGE_MASTER_PASS \
	$STAGE_DATABASE \
	--ssl-ca=config/amazon-rds-ca-cert.pem \
	--ssl-mode=VERIFY_IDENTITY