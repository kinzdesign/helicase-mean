#!/bin/bash
echo 'Pushing production data backup to staging...'
echo ' - database structure'
echo ' - seed data'
echo ' - production participant data backup'
cat sql/prod.structure.sql sql/prod.seeds.sql sql/prod.data.sql | \
mysql --comments \
	-h $STAGE_HOST \
	-P $STAGE_PORT \
	-u $STAGE_MASTER_USER \
	-p$STAGE_MASTER_PASS \
	$STAGE_DATABASE \
	--ssl-ca=config/amazon-rds-ca-cert.pem \
	--ssl-mode=VERIFY_IDENTITY
echo 'Done!'
echo