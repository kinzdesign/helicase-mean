#!/bin/bash
echo 'Backing up participant data from staging...'
mysqldump --no-create-info \
	-h $STAGE_HOST \
	-P $STAGE_PORT \
	-u $STAGE_MASTER_USER \
	-p$STAGE_MASTER_PASS \
	$STAGE_DATABASE \
	participants q_participant_quiz responses interactions \
	--ssl-ca=config/amazon-rds-ca-cert.pem \
	--ssl-mode=VERIFY_IDENTITY \
> sql/stage.data.sql
echo 'Done!'
echo
echo
echo 'Pushing data to staging...'
echo ' - database structure'
echo ' - seed data'
echo ' - staging participant data backup'
cat sql/structure.sql sql/seeds.sql sql/stage.data.sql | \
mysql --comments \
	-h $STAGE_HOST \
	-P $STAGE_PORT \
	-u $STAGE_MASTER_USER \
	-p$STAGE_MASTER_PASS \
	$STAGE_DATABASE \
	--ssl-ca=config/amazon-rds-ca-cert.pem \
	--ssl-mode=VERIFY_IDENTITY
echo 'Done!'
echo