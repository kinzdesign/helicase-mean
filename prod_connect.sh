#!/bin/bash
mysql \
	-h $PROD_HOST \
	-P $PROD_PORT \
	-u $PROD_MASTER_USER \
	-p$PROD_MASTER_PASS \
	$PROD_DATABASE \
	--ssl-ca=config/amazon-rds-ca-cert.pem \
	--ssl-mode=VERIFY_IDENTITY