#!/bin/bash
echo
echo 'Restoring helicase database structure to staging...'
mysql -h $STAGE_HOST \
	-P $STAGE_PORT \
	-u $STAGE_MASTER_USER \
	-p$STAGE_MASTER_PASS \
	--comments $STAGE_DATABASE \
< sql/stage.structure.sql
echo 'Done!'
echo
echo 'Restoring helicase seed data to staging...'
mysql -h $STAGE_HOST \
	-P $STAGE_PORT \
	-u $STAGE_MASTER_USER \
	-p$STAGE_MASTER_PASS \
	--comments $STAGE_DATABASE \
< sql/stage.seeds.sql
echo 'Done!'
echo
echo 'Restoring helicase participant data to staging...'
mysql -h $STAGE_HOST \
	-P $STAGE_PORT \
	-u $STAGE_MASTER_USER \
	-p$STAGE_MASTER_PASS \
	--comments $STAGE_DATABASE \
< sql/stage.data.sql
echo 'Done!'
echo