#!/bin/bash
echo 'Backing up participant data from production...'
mysqldump --no-create-info \
	-h $PROD_HOST \
	-P $PROD_PORT \
	-u $PROD_MASTER_USER \
	-p$PROD_MASTER_PASS \
	$PROD_DATABASE \
	participants q_participant_quiz responses interactions \
	--ssl-ca=config/amazon-rds-ca-cert.pem \
	--ssl-mode=VERIFY_IDENTITY \
> sql/prod.data.sql
echo 'Done!'
echo
echo
echo 'Pushing data to production...'
echo ' - database structure'
echo ' - seed data'
echo ' - production participant data backup'
cat sql/structure.sql sql/seeds.sql sql/prod.data.sql | \
mysql --comments \
	-h $PROD_HOST \
	-P $PROD_PORT \
	-u $PROD_MASTER_USER \
	-p$PROD_MASTER_PASS \
	$PROD_DATABASE \
	--ssl-ca=config/amazon-rds-ca-cert.pem \
	--ssl-mode=VERIFY_IDENTITY
echo 'Done!'
echo