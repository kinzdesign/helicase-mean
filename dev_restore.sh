#!/bin/bash
echo
echo 'Restoring helicase database structure to development...'
mysql -h $DEV_HOST \
	-P $DEV_PORT \
	-u $DEV_MASTER_USER \
	-p$DEV_MASTER_PASS \
	--comments $DEV_DATABASE \
< sql/structure.sql
echo 'Done!'
echo
echo 'Restoring helicase seed data to development...'
mysql -h $DEV_HOST \
	-P $DEV_PORT \
	-u $DEV_MASTER_USER \
	-p$DEV_MASTER_PASS \
	--comments $DEV_DATABASE \
< sql/seeds.sql
echo 'Done!'
echo
echo 'Restoring helicase participant data to development...'
mysql -h $DEV_HOST \
	-P $DEV_PORT \
	-u $DEV_MASTER_USER \
	-p$DEV_MASTER_PASS \
	--comments $DEV_DATABASE \
< sql/dev.data.sql
echo 'Done!'
echo